#!/usr/bin/env python

from time import sleep
from datetime import datetime
import numpy as np
from tqdm import trange
from epics import PV

from slic.gui import GUI
from slic.core.adjustable import Adjustable, PVAdjustable, DummyAdjustable
from slic.core.acquisition import SFAcquisition#, PVAcquisition
from slic.core.condition import PVCondition
from slic.core.scanner import Scanner
from slic.devices.general.motor import Motor
from slic.devices.general.delay_stage import DelayStage
from slic.devices.general.smaract import SmarActAxis
from slic.devices.general.shutter import Shutter
from slic.utils import devices, Marker, as_shortcut
#from slic.utils import Channels, Config, Elog, Screenshot, PV
#from slic.core.acquisition.fakeacquisition import FakeAcquisition
#from slic.utils import unpickle

from devices.attenuator import Attenuator
from devices.undulator import Undulators, TwoColorChicaneCurrent, TwoColorChicaneDelay, Phases
from devices.holo_sample_motion import HoloSample
#from devices.phases import UndPhases

from channels import channels, pvs, detectors
from spreadsheet import overview, print_overview, print_line_for_spreadsheet, spreadsheet_info
import colors


dummy = DummyAdjustable(units="au")

#gas_attenuator = PVAdjustable("SATFE10-VVRE054-A010:SET_VAL-UI", name="Gas Attenuator")

gas_attenuator = PVAdjustable("SATFE10-VVRE054-A010:SET_VAL-UI", process_time=20, name="Gas Attenuator Volts")

gas_attenuator_trans = PVAdjustable("SATFE10-OGAT053:TRANSMISSION", process_time=20, name="Gas Attenuator Transmission")

#jet_delay = PVAdjustable("SATES20-CVME-EVR0:Pul6-Delay-SP", "SATES20-CVME-EVR0:Pul6-Delay-RB", accuracy=1, name="Jet Delay")

source_delay = PVAdjustable("SATES20-CVME-EVR0:Pul12_NEW_DELAY", "SATES20-CVME-EVR0:Pul12_NEW_DELAY", accuracy=1, name="Source Delay")
vmi_gate = PVAdjustable("SATES20-CVME-EVR0:Pul11_NEW_DELAY", "SATES20-CVME-EVR0:Pul11_NEW_DELAY", accuracy=1, name="VMI Gate Delay")

mono = PVAdjustable("SATOP11-OSGM087:SetEnergy", "SATOP11-OSGM087:photonenergy", accuracy=0.1, process_time=3, name="Mono Coupled WORKING")

#waveplate = Motor("SLAAT21-LMOT-M702:MOT", name="Laser Waveplate (laser pulse energy)")
waveplate = Motor("SLAAT21-LMOT-M702:MOT", name="Laser Waveplate (pump laser T3)")
TT_delay = DelayStage("SLAAT21-LMOT-M704:MOT", name="Time tool delay")
#laser_comp21 = Motor("SLAAT21-LMOT-M701:MOT", name="Laser Comp T2")
#laser_comp11 = Motor("SLAAT21-LMOT-M705:MOT", name="Laser Comp T1 1")

pump_probe_delay2 = DelayStage("SLAAT21-LMOT-M703:MOT", name="pump_probe_delay_ch2")
#pump_probe_delay1 = DelayStage("SLAAT21-LMOT-M713:MOT", name="pump_probe_delay_ch1")

#laser_comp12 = Motor("SLAAT21-LMOT-M706:MOT", name="Laser Comp T1 2")
#laser_delay = DelayStage("SLAAT21-LMOT-M708:MOT", name="Laser Delay")
#laser_delay = Motor("SLAAT21-LMOT-M708:MOT", name="Laser Global Delay")
lxt = PVAdjustable("SLAAT01-LTIM-PDLY:DELAY", pvname_done_moving="SLAAT01-LTIM-PDLY:WAITING", name="LXT")

#source_y = Motor("SATES20-MANIP2:MOTOR_2", name="Needle y")
#source_x = Motor("SATES20-MANIP2:MOTOR_1", name="Needle x")


#injector_x = Motor("SATES20-MANIP1:MOTOR_1", name="Injector X")
#injector_y = Motor("SATES20-MANIP1:MOTOR_2", name="Injector Y")


pump_pulse_energy = Motor("SLAAT21-LMOT-M702:MOT", name="Pump Pulse Energy (Waveplate global)")

att = Attenuator("SATFE10-OATT064")
shutter = Shutter("SATOP21-OPSH138")

chic_delay_current = TwoColorChicaneCurrent(name="zz Two Color Chicane as current")
chic_delay_delay = TwoColorChicaneDelay(name="zz Two Color Chicane as delay")
chic_delay = chic_delay_current #TODO remove, and rename above

und = Undulators(name="z Athos Undulators (both colors)")
und1 = Undulators([6, 7, 8, 9, 10, 11, 12, 13],     n_und_ref=10, name="z Athos Undulators 06-13 (first color)")
und2 = Undulators([15, 16, 17, 18, 19, 20, 21, 22], n_und_ref=19, name="z Athos Undulators 15-22 (second color)")

und.set_limits(320, 1300)
und1.set_limits(320, 1300)
und2.set_limits(320, 1300)


class MonoUndCoupled(Adjustable):

    def __init__(self, mono, und, und_energy_offset=0):
        super().__init__("MONO-UND-COUPLED", name="z mono and undulators coupled", units="eV")
        self.mono = mono
        self.und = und
        self.und_energy_offset = und_energy_offset

    def get_current_value(self):
        return self.mono.get_current_value()

    def set_target_value(self, value):
        t1 = self.mono.set_target_value(value)
        value += self.und_energy_offset
        t2 = self.und.set_target_value(value)
        t1.wait()
        t2.wait()

    def is_moving(self):
        return self.mono.is_moving() or self.und.is_moving()



#mono_und_coupled = MonoUndCoupled(mono, und, und_energy_offset=14.0)



### Smaract stage at an angle in the Yaxis:
#mot_x = SmarActAxis("SATES21-XSMA166:MOT8", name="Table 1: flagx")
#mot_y = Converted(

m1 = PVAdjustable("SATES23-XSMA169:MOT7:DRIVE", internal=True)
m2 = PVAdjustable("SATES23-XSMA169:MOT8:DRIVE", internal=True)
#holo_sample_motion_y = HoloSample(m1, m2)
#holo_sample_motion_x = SmarActAxis("SATES23-XSMA169:MOT7", name="Table 3: Sample X")


#params = unpickle("devices/phases/UE38_meas_and_fit_data.pickle")["fitdata"]
#und_names = [f"SATUN{i:02}" for i in range(6, 22+1) if i != 14]
#polarization = UndPhases("SATUN-PHASES", params, und_names=und_names, isparallel=True, name="z Polarization")


#target_x = PVAdjustable("SATES10-CMOV-M004:MOT.VAL",     name="Streaking Target X")
#target_y = PVAdjustable("SATES10-CMOV-M005:MOT.VAL",     name="Streaking Target Y")
#target_z = PVAdjustable("SATES10-CMOV-M006:MOT.VAL",     name="Streaking Target Z")

#parabola_x = PVAdjustable("SATES10-CMOV-M007:MOT.VAL",     name="Streaking parabola X")
#parabola_y = PVAdjustable("SATES10-CMOV-M008:MOT.VAL",     name="Streaking parabola Y")
#parabola_z = PVAdjustable("SATES10-CMOV-M009:MOT.VAL",     name="Streaking parabola Z")
#parabola_R = PVAdjustable("SATES10-CMOV-M010:MOT.VAL",     name="Streaking parabola R")

#tof_x = PVAdjustable("SATES10-CMOV-M001:MOT.VAL",     name="Streaking TOF X")



xota1_z_trans = Motor("SATES21-XOTA166:W_Z",     name="Table 1: Z coordinated")
xota1_y_trans = Motor("SATES21-XOTA166:W_Y",     name="Table 1: Y coordinated")
#xota1_hori_z  = Motor("SATES21-XOTA166:MOTOR_Z", name="Table 1: Z raw")

xota3_z_trans = Motor("SATES23-XOTA169:W_Z",     name="Table 3: Z coordinated")
xota3_y_trans = Motor("SATES23-XOTA169:W_Y",     name="Table 3: Y coordinated")


mot3_x = Motor("SATES23-XSMA169:MOT_7", name="Table 3: sma x")
mot3_y = Motor("SATES23-XSMA169:MOT_8", name="Table 3: sma y")

#linear_detector = Motor("SATES23-XSMA169:MOT_13", name="Linear detector Z")

#mirror_tilt = PVAdjustable("SATES20-XSMA165:MOT17:DRIVE", name="Mirror_tilt")
#mirror_tip = PVAdjustable("SATES20-XSMA165:MOT16:DRIVE", name="Mirror_tip")


#Sample_Y = SmarActAxis("SATES23-XSMA169:MOT1", name="Holo sample Y")
#Sample_Z = SmarActAxis("SATES23-XSMA169:MOT2", name="Holo sample Z")
#Sample_X = SmarActAxis("SATES23-XSMA169:MOT3", name="Holo sample X")

standa = Motor("SLAAT21-LMOT-M707:MOT", name="Standa Motor")

mark_microscope = Marker(standa, 141.344, "Microscope")
mark_gascell    = Marker(standa,  88.334, "Gas Cell")
mark_tof        = Marker(standa,  28.330, "TOF")


#magn = PV("SATUN14-MBND100:CYCLE")

#@as_shortcut
#def cycle_magnet():
#    magn.put(1, wait=True)
#    for _ in trange(250):
#        sleep(1)
#    # set the current to 1 and 5 consecutively,
#    # since the results after the first step always looks strange
#    chic_delay.set(1).wait()
#    sleep(1)
#    chic_delay.set(5).wait()
#    sleep(1)


@as_shortcut
def cycle_magnet():
    chic_delay.cycle_magnet()



# add some more devices to the overview
overview.standa = standa
overview.TT_delay = TT_delay
overview.pump_probe_delay = pump_probe_delay2
overview.LXT = lxt



@as_shortcut
def unstuck_undulators():
    und.unstuck()



from slic.core.acquisition.broker import restapi

@as_shortcut
def power_on_modules():
    restapi.power_on_modules("http://sf-daq:10003", "JF15T08V01", [1, 3])

@as_shortcut
def power_off_modules():
    restapi.power_off_modules("http://sf-daq:10003", "JF15T08V01", [1, 3])



from slic.core.acquisition.spreadsheet import Spreadsheet

spreadsheet = Spreadsheet(spreadsheet_info, placeholders=["comment", "sample"], host="satese-cons-01.psi.ch", port=9090)



instrument = "maloja"
#pgroup = "p18493" # commissioning june 2020
#pgroup = "p18722" # commissioning sept 2020
#pgroup = "p18863" # JF commissioning april 2021
#pgroup = "p19241" # two colors
#pgroup = "p19342" # XMCD
#pgroup = "p19343" # slic dev
#pgroup = "p19373" # laser
#pgroup = "p19483" # pilot 1 -- TRXPS
#pgroup = "p19488" # pilot 2 -- Non-Linear Spectroscopy
#pgroup = "p19510" # pilot 2 -- Non-Linear Spectroscopy
#pgroup = "p19727" # Laser only timing tests
#pgroup = "p19509" # slic dev
#pgroup = "p19743" # Focus tests and timing tests
#pgroup = "p19582" # pilot 3 -- SPI
#pgroup = "p19583" # permalloy 2.0
#pgroup = "p20073" # alvise
#pgroup = "p20194" # TOF beamtime heated valve
#pgroup = "p20276" # COLTRIMS commissioning
#pgroup = "p20553" # liquid jet
#pgroup = "p20561" # HHGX beamtime
#pgroup = "p20568" # COLTRIMS commissioning
#pgroup = "p20562" # Circular dichroism -- Woerner
#pgroup = "p20560" # Four-wave-mixing -- Knopp
#pgroup = "p20279" # SPI -- Ban
#pgroup = "p20848" # Holo -- Boris 
#pgroup = "p20851" # Camera test Edwin, use below instead
#pgroup = "p21011" # Camera test Edwin
#pgroup = "p20823" # SPI - Issendorf
#pgroup = "p21108" # SPI commisioning multi-stage
#pgroup = "p21160" # Streaking HERO as
#pgroup = "p21201" # Heated jet XPS test
#pgroup = "p20824" # XMCD two color
#pgroup = "p21234" # glycine
#pgroup = "p21235" # CF4
#pgroup = "p21535" # Liquid Jet commissioning
#pgroup = "p21509" # HBT
#pgroup = "p21510" # Time-resolved ferromagnetic resonance
#pgroup = "p21584" # Transverse cavity and spike counting test
#pgroup = "p21512" # REMI: Molecular Clock -- Xinhua
#pgroup = "p21511" # XPS, Rolles
#pgroup = "p21638" # oven test and time tool, inhouse, coltrims
#pgroup = "p21624" #XPS Ingo Fischer 
#pgroup = "p21919" #wavefront tests
#pgroup = "p21626" #Skyrmion nucleation
#pgroup = "p21623" #XPS Artem
#pgroup = "p21625" #Holo VO2
#pgroup = "p21978" #streaking two tofs 
#pgroup = "p22175" #EtOH TOF
#pgroup = "p22190" # XPS Lorenzo
#pgroup = "p22224" #JF startup
#pgroup = "p22083" #Kling
pgroup = "p22224" #Photon spectrometer tests


daq = SFAcquisition(instrument, pgroup, default_channels=channels, default_pvs=pvs, default_detectors=detectors, rate_multiplicator=1, append_user_tag_to_data_dir=True, spreadsheet=spreadsheet)
#daq = FakeAcquisition(instrument, pgroup)

# tell sf-daq which PVs to monitor
daq.update_config_pvs()

#daqPV = PVAcquisition(instrument, pgroup, default_channels=channels_ks) # workaround for KS not going to DB

check_intensity = PVCondition("SATBD01-DBPM060:Q2", vmin=5, vmax=None, wait_time=1, required_fraction=0.8)
#check_intensity = None

scan = Scanner(scan_info_dir=f"/sf/{instrument}/data/{pgroup}/res/scan_info", default_acquisitions=[daq], condition=check_intensity)
gui = GUI(scan, show_goto=True, show_spec=True, show_run=True)



## CTA DAQ
#from ctadaq_skyrmion_nucleation import CTAAcquisition



##muphase = PVAdjustable("VXGC:Phase", name='Microwave Phase')
###mufreq = PV('VXGC:Freq')
#ctadaq = CTAAcquisition(instrument, pgroup, default_channels=channels, default_pvs=pvs, default_detectors=None, rate_multiplicator=1, append_user_tag_to_data_dir=True, spreadsheet=spreadsheet)
#ctascan = Scanner(scan_info_dir=f"/sf/{instrument}/data/{pgroup}/res/scan_info", default_acquisitions=[ctadaq], condition=check_intensity)
#ctagui = GUI(ctascan, show_goto=True, show_spec=True, show_run=True, title="CTA DAQ")


#from devices.adrian import turboscope, microwave


## holo scan script 
#def holo_scan():
#
#    energies = [
#        528.5,
#        529.2,
#        529.7,
#        530.2,
#        531.0,
#     ]
#
#    n_repeats = 1
#    reps = 10
#   
#    energies = sorted(sorted(set(energies)) * n_repeats)
#
#    for E in energies:
#
#        mono.set(E).wait()
#
#        for i in range(reps):
#            
#            printable_E = str(round(E, 1)).replace(".", "_")
#
#            fname = f"holo_scan_{printable_E}eV"
#            print(fname)
#
#            while check_intensity.wants_repeat():
#                daq.acquire(fname, n_pulses=10, is_scan_step=(i != 0)).wait()
#


