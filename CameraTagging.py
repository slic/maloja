#!/usr/bin/env python

print("Camera Tagging Script")

from epics import caput, caget, PV
import numpy as np
import time as time

#cam1="SATES21-CAMS154-M1"
#cam2="SATES24-CAMS161-M1"

#cam1xs = PV(cam1+':REGIONX_START')
#cam1xe = PV(cam1+':REGIONX_END')
#cam1ys = PV(cam1+':REGIONY_START')
#cam1ye = PV(cam1+':REGIONY_END')
#cam1ye = PV(cam1+':STATUS')

#cam2xs = PV(cam2+':REGIONX_START')
#cam2xe = PV(cam2+':REGIONX_END')
#cam2ys = PV(cam2+':REGIONY_START')
#cam2ye = PV(cam2+':REGIONY_END')
#cam2ye = PV(cam2+':STATUS')
def camera_tag(daq):
    def create_cam(cam_name):
        cam_dict = {
                    'name':cam_name,
                    'xs':PV(cam_name+':REGIONX_START'),
                    'xe':PV(cam_name+':REGIONX_END'),
                    'ys':PV(cam_name+':REGIONY_START'),
                    'ye':PV(cam_name+':REGIONY_ENDimport'),
                    'status':PV(cam_name+':CAMERASTATUS'),
                    'cap':PV(cam_name+':CAPTURE_OK')
        }

        print('status:', cam_dict['status'].get())

        return cam_dict


    def changeROI(cam,xs,xe,ys,ye):
        cam['status'].put(1)
        time.sleep(.5)
        cam['xs'].put(xs)
        time.sleep(.5)
        cam['xe'].put(xe)
        time.sleep(.5)
        cam['ys'].put(ys)
        time.sleep(.5)
        cam['ye'].put(ye)
        time.sleep(.5)
        cam['status'].put(2)
        counter = 0
        while cam['cap'].get() < 10:
            print('Waiting...')
            time.sleep(.5)
            conter += 1
            if counter > 10:
                print('Still waiting... Setting status again')
                cam['status'].put(2)
                counter = 0
        print('Camera set')
        
    def calcROI(cam, mode,percent): #0=centered  1=horiz
        maxX=cam["maxX"]
        maxY=cam["maxY"]
        ye=2560
        ys=1
        xs=1
        xe=2048
        if mode==0:  #square centered
            factor=np.sqrt(percent/100)/2
            xs=1+maxX*(0.5-factor)
            xe=1+maxX*(0.5+factor)
            ys=1+maxY*(0.5-factor)
            ye=1+maxY*(0.5+factor)

        if mode==1:  #squashed horizontally (tall and narrow) portrait
            factor=(percent/200)
            xs=1+maxX*(0.5-factor)
            xe=1+maxX*(0.5+factor)
            ys=1
            ye=maxY

        if mode==2:  #squashed vertically (wide) landscape
            factor=(percent/200)
            xs=1
            xe=maxX
            ys=1+maxY*(0.5-factor)
            ye=1+maxY*(0.5+factor)

        if mode==3:  #squashed horizontally (tall and narrow) portrait with 50pix crop top and bottom
            factor=(percent/200)
            xs=1+maxX*(0.5-factor)
            xe=1+maxX*(0.5+factor)
            ys=50
            ye=maxY-50

        vfill=round(100*(ye-ys+1)/maxY)
        return xs,xe,ys,ye,vfill


    cam1 = create_cam('SATES21-CAMS154-M1')
    cam2 = create_cam('SATES24-CAMS161-M1')
    cam3 = create_cam('SATES21-CAMS-PATT1')
    cam4 = create_cam('SATES21-CAMS154-M2')
    #cam1 = create_cam('SARFE10-PSSS059')

    cam1["scanPoints"]=[10,25,40,50,65,80, 100]
    cam2["scanPoints"]=[10,25,40,50,65,80, 100]
    cam4["scanPoints"]=[10,25,40,50,65,80, 100]

    #cam1["scanPoints"]=[10,20,30,40,50,60,70,80,90]
    #cam2["scanPoints"]=[10,25,40,55,70,85]
    modesToScan       =[1,2,0,3]

    cam1["maxX"]      =2560
    cam1["maxY"]      =2160
    cam2["maxX"]      =2560
    cam2["maxY"]      =2160
    cam4["maxX"]      =2560
    cam4["maxY"]      =2160

    mode = 0
    n_shots = 2500
    scantype=3     #0 two camera scan
                   #1 mode scan with 1 camera
                   #2 spot measurement

    if scantype==0:
        for i in cam1['scanPoints']:

            xs,xe,ys,ye,vf1=calcROI(cam1,mode,i)       
            changeROI (cam1,xs,xe,ys,ye)

            for j in cam2['scanPoints']:
                fn = f"test_cam1_{i}_cam2_{j}_mode_{mode}"
                print(fn)
                xs,xe,ys,ye,vf2=calcROI(cam2, mode,j)       
                changeROI (cam2,xs,xe,ys,ye)
                #PV("SGE-CPCW-C1-EVR0:Pul0-Delay-SP").put(10500-j*100)
                #PV("SGE-CPCW-C1-EVR0:Pul14-Delay-SP").put(10500-i*100)
                PV("SGE-CPCW-C1-EVR0:Pul0-Delay-SP").put(10500-vf2*100)
                PV("SGE-CPCW-C1-EVR0:Pul14-Delay-SP").put(10500-vf1*100)
                if vf1>40:
                    PV("SATES21-CAMS154-M1:SW_PULSID_SRC").put(5)   #5=9000uS 
                else:
                    PV("SATES21-CAMS154-M1:SW_PULSID_SRC").put(2)   #3=5500uS (2=4500uS)

                if vf2>40:
                    PV("SATES24-CAMS161-M1:SW_PULSID_SRC").put(6)   #6=9000uS 
                else:
                    PV("SATES24-CAMS161-M1:SW_PULSID_SRC").put(3)   #4=5500uS (3=4500uS)

                print(10500-vf1*100);print(10500-vf2*100)
                time.sleep(30)
                daq.acquire(fn, n_pulses=n_shots)

                time.sleep(5)

    if scantype==1:   #Mode scan
        for i in modesToScan:                  #i is the mode
            for j in cam1['scanPoints']:       #j is the fill %age
                fn = f"test_cam1_{j}_mode_{i}_"
                print(fn)
                xs,xe,ys,ye,vf1=calcROI(cam1,i,j)       
                changeROI (cam1,xs,xe,ys,ye)
                PV("SGE-CPCW-C1-EVR0:Pul14-Delay-SP" ).put(10500-vf1*100)
                if vf1>40:
                    PV("SATES21-CAMS154-M1:SW_PULSID_SRC").put(5)   #5=9000uS 
                else:
                    PV("SATES21-CAMS154-M1:SW_PULSID_SRC").put(2)   #3=5500uS (2=4500uS)

                print(10500-vf1*100);
                time.sleep(30)
                daq.acquire(fn, n_pulses=n_shots)

                time.sleep(5)

    if scantype==2:   #single point
        fn = f"test_cam1_single_pt_"
        daq.acquire(fn, n_pulses=n_shots)

    if scantype==3:   #Mode scan
        for i in modesToScan:                  #i is the mode
            for j in cam4['scanPoints']:       #j is the fill %age
                fn = f"test_cam1_{j}_mode_{i}_"
                print(fn)
                xs,xe,ys,ye,vf1=calcROI(cam4,i,j)
                changeROI (cam4,xs,xe,ys,ye)
                PV("SGE-CPCW-C4-EVR0:Pul01-Delay-SP" ).put(10500-vf1*100)
                if vf1>40:
                    PV("SATES21-CAMS154-M1:SW_PULSID_SRC").put(5)   #5=9000uS 
                else:
                    PV("SATES21-CAMS154-M1:SW_PULSID_SRC").put(2)   #3=5500uS (2=4500uS)

                print(10500-vf1*100);
                time.sleep(30)
                daq.acquire(fn, n_pulses=n_shots)

                time.sleep(5)


    #xs,xe,ys,ye=calcROI(cam1,2,20)
    #print(xs, xe, ys, ye)
    #print('Width ', xe-xs)
    #print('Height', ye-ys)

    #changeROI(cam1,xs,xe,ys,ye)

    #time.sleep(10)

    #xs,xe,ys,ye=calcROI(cam1,2,80)       
    #changeROI(cam1,xs,xe,ys,ye)
    #print(xs, xe, ys, ye)
    #print('Width ', xe-xs)
    #print('Height', ye-ys)

    #time.sleep(10)

    #xs,xe,ys,ye=calcROI(cam1,2,100)       
    #changeROI(cam1,xs,xe,ys,ye)
    #print(xs, xe, ys, ye)
    #print('Width ', xe-xs)
    #print('Height', ye-ys)













