from slic.core.adjustable import PVAdjustable 


class Magnet(PVAdjustable):

    def __init__(self, name="Simone's Magnet", accuracy=1):
        pvname_setvalue = "SATES20-ES1-USER2:DAT"
        pvname_readback = "SATES20-ES1-USER1:DAT"
        super().__init__(pvname_setvalue, pvname_readback=pvname_readback, name=name, accuracy=accuracy)


class Frequency(PVAdjustable):

    def __init__(self, name="Simone's Frequency", process_time=5):
        pvname_setvalue = "SATES20-ES1-USER2:DAT"
        super().__init__(pvname_setvalue, name=name, process_time=process_time)



