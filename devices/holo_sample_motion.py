import numpy as np

from slic.core.adjustable import Adjustable, Converted, Combined
from slic.core.device import SimpleDevice


FACTOR2 = 1/np.sin(30 * np.pi/180)
FACTOR1 = -np.cos(30 * np.pi/180)


class HoloSample(Adjustable):

    def __init__(self, m1, m2, ID="HOLOGRAPHY-SAMPLE", name="Holography sample Y", units="mm"):
        super().__init__(ID, name=name, units=units)
        self.m1 = m1
        self.m2 = m2
        #self.adjs = SimpleDevice(ID, combined=self, m1=m1, m2=m2)


    # one master, others are following
    def get_current_value(self):
        return self.m2.get_current_value()/FACTOR2

    # current value is average of all
    #def get_current_value(self):
    #    converted_values = [
    #        self.m1.get_current_value(),
    #        self.m2.get_current_value()
    #    ]
    #    return np.mean(converted_values)


    def set_target_value(self, value):
        #print('current val m1', self.m1.get_current_value())
        #print('current val m2', self.m2.get_current_value())
        #print('F1: %f F2 %f'%(FACTOR1, FACTOR2) )
        #print('requested val: ', value)

        
        tasks = [
            self.m1.set_target_value(value * FACTOR1),
            self.m2.set_target_value(value * FACTOR2)
        ]
        
        for t in tasks:
            t.wait()


    def is_moving(self):
        return self.m1.is_moving() or self.m2.is_moving()



#def g1(x): return x / FACTOR1
#def g2(x): return x / FACTOR2
#def s1(x): return x * FACTOR1
#def s2(x): return x * FACTOR2

#m1 = SmarActAxis("SATES21-XSMA166:MOT7", internal=True)
#m2 = SmarActAxis("SATES21-XSMA166:MOT8", internal=True)
#c1 = Converted("C1", m1, g1, s1, internal=True)
#c2 = Converted("C2", m2, g2, s2, internal=True)
#comb = Combined("COMBINED", (c1, c2), name="Holo")



