from slic.core.adjustable import PVAdjustable
from slic.core.device import SimpleDevice


turboscope = SimpleDevice("Turboscope",
    current_frame           = PVAdjustable("DPO:CurrentFrame"),
    demodulation_frequency  = PVAdjustable("DPO:DemodulationFrequency"),
    frame_number            = PVAdjustable("DPO:FrameNumber")
)

microwave = SimpleDevice("Microwave",
    freq        = PVAdjustable("VXGC:Freq"),
    lock_status = PVAdjustable("VXGC:LockStatus"),
    phase       = PVAdjustable("VXGC:Phase")
)



