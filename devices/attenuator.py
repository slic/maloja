from time import sleep
from types import SimpleNamespace

from slic.utils import typename
from slic.core.task import Task


import epics

class PV(epics.PV):

    def put(self, *args, wait=True, **kwargs):
        super().put(*args, wait=wait, **kwargs)



class Attenuator:

    def __init__(self, name, n_motors=6):
        if not name.endswith(":"):
            name += ":"
        self.name = name
        self.pvs = SimpleNamespace(
            energy = PV(name + "ENERGY"),
            transmission = PV(name + "TRANS_SP"),
            motors_done_moving = [PV(name + "MOTOR_{}.DMOV".format(i+1)) for i in range(n_motors)]
        )


    def get_current_value(self):
        return self.get_transmission()

    def set_target_value(self, value):
        def changer():
            self.set_transmission(value)
        return Task(changer)


    def set_transmission(self, value):
        self.pvs.transmission.put(value)
        self.wait_for_motors()

    def get_transmission(self):
        return self.pvs.transmission.get()

    transmission = property(get_transmission, set_transmission)


    def set_energy(self, value):
        self.pvs.energy.put(value)

    def get_energy(self):
        return self.pvs.energy.get()

    energy = property(get_energy, set_energy)


    def motors_are_moving(self):
        is_moving = lambda dmov: not bool(dmov.get())
        return any(is_moving(dmov) for dmov in self.pvs.motors_done_moving)

    def wait_for_motors(self):
        while self.motors_are_moving():
            sleep(0.01)


    def __repr__(self):
        tn = typename(self)
        trans = self.get_transmission() * 100
        energ = self.get_energy()
        return "{}(\"{}\") at {}% transmission for {} eV".format(tn, self.name, trans, energ)



