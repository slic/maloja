from slic.devices.general.shutter import Shutter
from slic.utils.run_later import run_at, tomorrow

sh1 = Shutter("SATFE10-OPSH066")
sh2 = Shutter("SATOP21-OPSH138")

def close():
    sh1.close()
    sh2.close()

def klappezu():
    run_at(tomorrow(7), close)


