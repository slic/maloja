from time import sleep
from types import SimpleNamespace
from .waiting_epics import PV, caput, caget
from .utils import intify


class CameraClient:

    def __init__(self, name):
        self.name = name
        pv_roi_xmin = PV(name + ":REGIONX_START")
        pv_roi_xmax = PV(name + ":REGIONX_END")
        pv_roi_ymin = PV(name + ":REGIONY_START")
        pv_roi_ymax = PV(name + ":REGIONY_END")

        self.pvs = SimpleNamespace(
            xmin=pv_roi_xmin,
            xmax=pv_roi_xmax,
            ymin=pv_roi_ymin,
            ymax=pv_roi_ymax
        )

        self.max_roi = self.get_max_roi()

    def __repr__(self):
        tn = type(self).__name__
        fn = self.name
        return f"{tn}(\"{fn}\")"

    @property
    def status(self):
        head = repr(self)
        print(head)
        print("-" * len(head))
        channels = ("WARNCODE", "ERRCODE", "STATUSCODE", "BUSY", "CAMRATE", "FILERATE")
        maxlen = max(len(ch) for ch in channels)
        for ch in channels:
            fch = self.name +":" + ch
            val = caget(fch)

            line = ch + ": "
            line = line.ljust(maxlen + 2)
            line += str(val)
            print(line)

        print("-" * len(head))
        roi  = self.get_roi()
        full = self.get_max_roi()
        print(f"roi:  {roi}")
        print(f"full: {full}")


    def restart(self):
        self.off()
        self.offline()
        self.init()
        sleep(0.01)
        self.running()

    def offline(self):
        caput(self.name + ":INIT", 0) # OFFLINE

    def init(self):
        caput(self.name + ":INIT", 1) # INIT

    def off(self):
        caput(self.name + ":CAMERA", 0) # OFF

    def running(self):
        caput(self.name + ":CAMERA", 1) # RUNNING

    def reset_roi(self):
        caput(self.name + ":RESETROI.PROC", 1) # Reset ROI

    def set_parameters(self):
        caput(self.name + ":SET_PARAM", 1) # Set Parameters

    def clear_buffer(self):
        caput(self.name + ":CLEARMEM", 1) # Clear Buffer

    def width(self):
        res = caget(self.name + ":WIDTH")
        if res is None:
            raise RuntimeError("Could not caget width")
        return int(res)

    def height(self):
        res = caget(self.name + ":HEIGHT")
        if res is None:
            raise RuntimeError("Could not caget height")
        return int(res)

    def get(self):
        waveform = caget(self.name + ":FPICTURE")
        if waveform is None:
            raise RuntimeError("Could not caget waveform")
        width = self.width()
        height = self.height()
        length = width * height
        waveform = waveform[:length]
        image = waveform.reshape(height, width)
        return image

    def get_max_roi(self):
        current_roi = self.get_roi()
        self.reset_roi()
        max_roi = self.get_roi()
        self._set_roi(*current_roi)
        return max_roi

    def get_roi(self):
        xmin = self.pvs.xmin.get()
        xmax = self.pvs.xmax.get()
        ymin = self.pvs.ymin.get()
        ymax = self.pvs.ymax.get()
        return intify(xmin, xmax, ymin, ymax)


    def set_roi(self, xmin, xmax, ymin, ymax, debug=False):
        if debug:
            asked = (xmin, xmax, ymin, ymax)

        xminmin, xmaxmax, yminmin, ymaxmax = self.max_roi

        xmin = max(xmin, xminmin)
        xmax = min(xmax, xmaxmax)
        ymin = max(ymin, yminmin)
        ymax = min(ymax, ymaxmax)

        ymindelta = ymin - yminmin
        ymaxdelta = ymaxmax - ymax
        ydelta = min(ymindelta, ymaxdelta)

        ymin = yminmin + ydelta
        ymax = ymaxmax - ydelta

        if debug:
            adjusted = (xmin, xmax, ymin, ymax)
            print("   ", asked, "\n-> ", adjusted, ":", ydelta, ymindelta, ymaxdelta)

        self._set_roi(xmin, xmax, ymin, ymax, debug=debug)


    def _set_roi(self, xmin, xmax, ymin, ymax, debug=False):
        xmin, xmax, ymin, ymax = intify(xmin, xmax, ymin, ymax)

        self.off()

        self.pvs.xmin.put(xmin)
        self.pvs.xmax.put(xmax)
        self.pvs.ymin.put(ymin)
        self.pvs.ymax.put(ymax)

        self.set_parameters()
        self.clear_buffer()
        self.running()

        if debug:
            print("-->", self.get_roi(), "\n")



