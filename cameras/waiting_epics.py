import epics
from epics import caget


def caput(*args, wait=True, **kwargs):
    return epics.caput(*args, wait=wait, **kwargs)


class PV(epics.PV):

    def put(self, *args, wait=True, **kwargs):
        super().put(*args, wait=wait, **kwargs)


