from slic.core.adjustable import PVAdjustable, PVEnumAdjustable
from slic.core.device import SimpleDevice
from slic.utils import as_shortcut


class PVStringAdjustable(PVAdjustable):
    def get_current_value(self):
        return str(self.pvs.readback.get(as_string=True)).strip()



n_unds = [
    6, 7, 8, 9, 10, 11, 12, 13, # 14 is the CHIC
    15, 16, 17, 18, 19, 20, 21, 22
]

undulator_info = {}
for i in n_unds:
    undulator_info[f"energy{i}"]       = PVAdjustable(f"SATUN{i:02}-UIND030:FELPHOTENE",  internal=True)
    undulator_info[f"polarisation{i}"] = PVEnumAdjustable(f"SATUN{i:02}-UIND030:POL-SET", internal=True)


spreadsheet_info = dict(
#    standa = standa,
#    exp_delay = exp_delay,
#    laser_delay = laser_delay,
#    LXT = lxt,

    timeStamp          = PVAdjustable("SF-CPCL-TIM:TIME", internal=True),

    FELrepRate         = PVAdjustable("SWISSFEL-STATUS:Bunch-2-Appl-Freq-RB", internal=True),
    photonEnergy       = PVAdjustable("SATUN:FELPHOTENE", internal=True),
    pulse_energy       = PVAdjustable("SATFE10-PEPG046:PHOTON-ENERGY-PER-PULSE-AVG", internal=True),
#    mono_energy       = PVAdjustable("SATOP11-OSGM087:photonenergy", internal=True),

#    Scintillator            = PVAdjustable("SATES21-XSHV166:V-1-S-CH13", internal=True),
#    MCPb            = PVAdjustable("SATES21-XSHV166:V-1-S-CH14", internal=True),
#    Repellor            = PVAdjustable("SATES21-XSHV166:V-1-S-CH15", internal=True),
#    PMT            = PVAdjustable("SATES21-XSHV166:V-0-S-CH13", internal=True),
#    MCPf            = PVAdjustable("SATES21-XSHV166:V-0-S-CH14", internal=True),
#    Extractor            = PVAdjustable("SATES21-XSHV166:V-0-S-CH14", internal=True),

#    PaddleX_ch1        = PVAdjustable("SATES21-XSMA166:MOT_1.RBV", internal=True),
#    PaddleZ_ch1        = PVAdjustable("SATES21-XSMA166:MOT_2.RBV", internal=True),
#    PaddleY_ch1        = PVAdjustable("SATES21-XSMA166:MOT_3.RBV", internal=True),

#    BlockX_ch1        = PVAdjustable("SATES21-XSMA166:MOT_4.RBV", internal=True),
#    BlockY_ch1        = PVAdjustable("SATES21-XSMA166:MOT_5.RBV", internal=True),
#    BlockZ_ch1        = PVAdjustable("SATES21-XSMA166:MOT_6.RBV", internal=True),

#    Standa_TOF        = PVAdjustable("SLAAT21-LMOT-M711:MOT.RBV", internal=True),

#    NeedleZ_ch1    = PVAdjustable("SATES20-CMOV-M001:MOT.RBV", internal = True),
#    NeedleY_ch1    = PVAdjustable("SATES20-CMOV-M002:MOT.RBV", internal = True),
#    NeedleX_ch1    = PVAdjustable("SATES20-CMOV-M003:MOT.RBV", internal = True),

#    JetX_ch1           = PVAdjustable("SATES20-MANIP6:MOTOR_1.RBV", internal = True),
#    JetZ_ch1           = PVAdjustable("SATES20-MANIP6:MOTOR_2.RBV", internal = True),
#    JetY_ch1           = PVAdjustable("SATES20-MANIP6:MOTOR_3.RBV", internal = True),

#    Tablechamber1X             = PVAdjustable("SATES21-XOTA166:W_X.RBV", internal=True),
#    Tablechamber1Y             = PVAdjustable("SATES21-XOTA166:W_Y.RBV", internal=True),
#    Tablechamber1Z             = PVAdjustable("SATES21-XOTA166:W_Z.RBV", internal=True),



#    PaddleChamber1MaskX    = PVAdjustable("SATES21-XSMA166:MOT7:MOTRBV", internal=True),
#    PaddleChamber1MaskY    = PVAdjustable("SATES21-XSMA166:MOT8:MOTRBV", internal=True),
#    PaddleChamber1FrameX    = PVAdjustable("SATES21-XSMA166:MOT4:MOTRBV", internal=True),
#    PaddleChamber1YagY   = PVAdjustable("SATES21-XSMA166:MOT6:MOTRBV", internal=True),
#    PaddleChamber1z    = PVAdjustable("SATES21-XSMA166:MOT3:MOTRBV", internal=True), 
#    PaddleChamber1YagX   = PVAdjustable("SLAAT21-LMOT-M711:MOT.RBV", internal=True),

#    JetRot    = PVAdjustable("SATES23-XSMA169:MOT6:MOTRBV", internal=True),
#    JetX    = PVAdjustable("SATES23-XSMA169:MOT7:MOTRBV", internal=True),
#    JetY    = PVAdjustable("SATES23-XSMA169:MOT8:MOTRBV", internal=True),
#    JetZ    = PVAdjustable("SATES23-XSMA169:MOT9:MOTRBV", internal=True),

#    paddleX    = PVAdjustable("SATES23-XSMA169:MOT5:MOTRBV", internal=True),
#    paddleRot    = PVAdjustable("SATES23-XSMA169:MOT4:MOTRBV", internal=True),

#    pressChamb1        = PVAdjustable("SATES21-VM-VT1010:PRESSURE", internal=True),
#    pressJet            = PVAdjustable("SATES21-VM-VT1020:PRESSURE", internal=True),
#    pressHemisphere        = PVAdjustable("SATES21-VM-VT2010:PRESSURE", internal=True),
#    pressChamb2        = PVAdjustable("SATES21-VM-VT2020:PRESSURE", internal=True),
#    pressChamb2Skim        = PVAdjustable("SATES21-VM-VT2030:PRESSURE", internal=True),
    pressChamb3        = PVAdjustable("SATES21-VM-VT3010:PRESSURE", internal=True),
    pressJF        = PVAdjustable("SATES21-VM-VT3020:PRESSURE", internal=True),
#    pressNeedle      = PVAdjustable("SATES21-VM-VT3030:PRESSURE", internal=True),
#    pressSmallChamber = PVAdjustable("SATES21-VM-VT3030:PRESSURE", internal=True),
#    pressChamb3GasCell = PVAdjustable("SATES21-VM-VT3040:PRESSURE", internal=True),

    pumpProbeDelay          = PVAdjustable("SLAAT21-LMOT-M703:MOT.RBV", internal=True),
#    Timedelay3          = PVAdjustable("SLAAT21-LMOT-M704:MOT.RBV", internal=True),
#    Timedelay4          = PVAdjustable("SLAAT21-LMOT-M713:MOT.RBV", internal=True),
#    Waveplate2          = PVAdjustable("SLAAT21-LMOT-M712:MOT.RBV", internal=True),

    LXT                 = PVAdjustable("SLAAT01-LTIM-PDLY:DELAY", internal=True),

    wavePlate1         = PVAdjustable("SLAAT21-LMOT-M702:MOT.RBV", internal=True),
#    wavePlate2         = PVAdjustable("SLAAT21-LMOT-M712:MOT.RBV", internal=True),

#    inc_mirror_X     = PVAdjustable("SATES23-XSMA169:MOT_4.RBV", internal=True),###
#    inc_mirror_Z     = PVAdjustable("SATES23-XSMA169:MOT_5.RBV", internal=True),
#    inc_mirror_tilt         = PVAdjustable("SATES22-XSMA168:MOT_17.RBV", internal=True),

#    filter_X     = PVAdjustable("SATES23-XSMA169:MOT_13.RBV", internal=True),###
#    filter_Y     = PVAdjustable("SATES23-XSMA169:MOT_14.RBV", internal=True),

#    slit_upstream     = PVAdjustable("SATES24-XSMA171:MOT_2.RBV", internal=True),###
#    slit_top          = PVAdjustable("SATES24-XSMA171:MOT_3.RBV", internal=True),
#    slit_downstream   = PVAdjustable("SATES24-XSMA171:MOT_4.RBV", internal=True),###
#    slit_bottom       = PVAdjustable("SATES24-XSMA171:MOT_5.RBV", internal=True),

#    SmartActTTx        = PVAdjustable("SATES22-XSMA168:MOT_10.RBV", internal=True),
#    SmartActTTy        = PVAdjustable("SATES22-XSMA168:MOT_11.RBV", internal=True),
#    SmartActTTz        = PVAdjustable("SATES22-XSMA168:MOT_12.RBV", internal=True),

    #trigger_digitizer_ev     = PVAdjustable("SATES20-CVME-EVR0:Pul7_NEW_EV", internal=True),
    #trigger_digitizer_delay  = PVAdjustable("SATES20-CVME-EVR0:Pul7_NEW_DELAY", internal=True),
    #trigger_jet_ev     = PVAdjustable("SATES20-CVME-EVR0:Pul14_NEW_EV", internal=True),
    #trigger_jet_delay  = PVAdjustable("SATES20-CVME-EVR0:Pul14_NEW_DELAY", internal=True),


#    APDlinear        = PVAdjustable("SATES21-XSMA166:MOT_1.RBV", internal=True),

#    SampleZ        = PVAdjustable("SATES21-XSMA166:MOT_10.RBV", internal=True),
#    SampleX        = PVAdjustable("SATES21-XSMA166:MOT_11.RBV", internal=True),
#    SampleY        = PVAdjustable("SATES21-XSMA166:MOT_12.RBV", internal=True),



#    energy1            = PVAdjustable("SATUN06-UIND030:FELPHOTENE", internal=True),
#    energy2            = PVAdjustable("SATUN15-UIND030:FELPHOTENE", internal=True),
#    chicane_current_rb = PVAdjustable("SATUN14-MBND100:I-READ", internal=True),
#    chicane_current_sv = PVAdjustable("SATUN14-MBND100:I-SET", internal=True),

#    manip2needleESy    = PVAdjustable("SATES20-MANIP2:MOTOR_2.VAL", internal=True),
#    manip3ESy    = PVAdjustable("SATES20-MANIP3:MOTOR_2.VAL", internal=True),
#    manip7ESy    = PVAdjustable("SATES20-MANIP7:MOTOR_1.VAL", internal=True),
#    manip2needleESy    = PVAdjustable("SATES20-MANIP2:MOTOR_2.VAL", internal=True),
#    manip2needleESz    = PVAdjustable("SATES20-MANIP2:MOTOR_3.VAL", internal=True),
#    pol1               = PVEnumAdjustable("SATUN06-UIND030:POL-SET", internal=True),
#    pol2               = PVEnumAdjustable("SATUN15-UIND030:POL-SET", internal=True),
#    standaChamber1   = PVAdjustable("SLAAT21-LMOT-M707:MOT.RBV", internal=True),
#    standaChamber3   = PVAdjustable("SLAAT21-LMOT-M711:MOT.RBV", internal=True),

#    MicroscopeX    = PVAdjustable("SATES23-XSMA169:MOT10:MOTRBV", internal=True),
#    MicroscopeY    = PVAdjustable("SATES23-XSMA169:MOT11:MOTRBV", internal=True),
#    MicroscopeZ    = PVAdjustable("SATES23-XSMA169:MOT12:MOTRBV", internal=True),

    #Manip5Microscope_x      = PVAdjustable("SATES20-MANIP5:MOTOR_1.RBV", internal = True),
    #Manip5Microscope_y      = PVAdjustable("SATES20-MANIP5:MOTOR_2.RBV", internal = True),
    #Manip5Microscope_z      = PVAdjustable("SATES20-MANIP5:MOTOR_3.RBV", internal = True),

#    Manip6Top_x           = PVAdjustable("SATES20-MANIP6:MOTOR_1.RBV", internal = True),
#    Manip6Top_y           = PVAdjustable("SATES20-MANIP6:MOTOR_2.RBV", internal = True),
#    Manip6Top_z           = PVAdjustable("SATES20-MANIP6:MOTOR_3.RBV", internal = True),

#    Manip7Paddle_x           = PVAdjustable("SATES20-MANIP7:MOTOR_1.RBV", internal = True),
#    Manip7Paddle_y           = PVAdjustable("SATES20-MANIP7:MOTOR_2.RBV", internal = True),
#    Manip7Paddle_z           = PVAdjustable("SATES20-MANIP7:MOTOR_3.RBV", internal = True),

#    targetX             = PVAdjustable("SATES10-CMOV-M004:MOT.RBV", internal = True),
#    targetY             = PVAdjustable("SATES10-CMOV-M005:MOT.RBV", internal = True),
#    targetZ             = PVAdjustable("SATES10-CMOV-M006:MOT.RBV", internal = True),

#    parabolaX             = PVAdjustable("SATES10-CMOV-M007:MOT.RBV", internal = True),
#    parabolaY             = PVAdjustable("SATES10-CMOV-M008:MOT.RBV", internal = True),
#    parabolaZ             = PVAdjustable("SATES10-CMOV-M009:MOT.RBV", internal = True),
#    parabolaRot             = PVAdjustable("SATES10-CMOV-M010:MOT.RBV", internal = True),

#    tofX             = PVAdjustable("SATES10-CMOV-M001:MOT.RBV", internal = True),
#    tofY             = PVAdjustable("SATES10-CMOV-M002:MOT.RBV", internal = True),
#    tofZ             = PVAdjustable("SATES10-CMOV-M003:MOT.RBV", internal = True),

#    TempSample             = PVAdjustable("SATES20-CWAG-GEP01:TEMP09", internal = True),
#    TempTip                = PVAdjustable("SATES20-CWAG-GEP01:TEMP10", internal = True),
#    TempColdFinger         = PVAdjustable("SATES20-CWAG-GEP01:TEMP11", internal = True),


#    Manip4_x           = PVAdjustable("SATES20-MANIP4:MOTOR_1.RBV", internal = True),
#    Manip4_y           = PVAdjustable("SATES20-MANIP4:MOTOR_3.RBV", internal = True),

#    LshapeDoorX    = PVAdjustable("SATES24-XSMA171:MOT7:MOTRBV", internal=True),
#    LshapeDoorY    = PVAdjustable("SATES24-XSMA171:MOT8:MOTRBV", internal=True),
#    YagX    = PVAdjustable("SATES24-XSMA171:MOT9:MOTRBV", internal=True),
#    YagY    = PVAdjustable("SATES24-XSMA171:MOT10:MOTRBV", internal=True),

#    pressure_sample    = PVAdjustable("SATES21-VM-VT2020:PRESSURE", internal=True),
#    pressure_hemi      = PVAdjustable("SATES21-VM-VT2010:PRESSURE", internal=True),
#    temp_tip      = PVAdjustable("SATES20-CWAG-GEP01:TEMP08", internal=True),
#    temp_sample      = PVAdjustable("SATES20-CWAG-GEP01:TEMP07", internal=True),
    
#    PressureGasAtt= PVAdjustable("ATFE10-VMCP054-A050:PRESSURE", internal=True),
#    VoltsSetGasAtt= PVAdjustable("SATFE10-VVRE054-A010:SET_VAL-UI", internal=True),


#    TOF_ext_neg           = PVAdjustable("SATES21-XSHV166:V-0-S-CH13", internal=True),
#    TOF_Behlke_lessneg    = PVAdjustable("SATES21-XSHV166:V-0-S-CH14", internal=True),
#    TOF_MCP_front         = PVAdjustable("SATES21-XSHV166:V-0-S-CH15", internal=True),
#    TOF_ext_pos           = PVAdjustable("SATES21-XSHV166:V-1-S-CH13", internal=True),
#    TOF_rep_pos           = PVAdjustable("SATES21-XSHV166:V-1-S-CH14", internal=True),
#    TOF_unused            = PVAdjustable("SATES21-XSHV166:V-1-S-CH15", internal=True),

#    BiasAPD      = PVAdjustable("SATES21-XSHV166:V-0-S-CH2", internal=True),
#    BiasOtherPD      = PVAdjustable("SATES21-XSHV166:V-1-S-CH2", internal=True),

#    V_remi             = PVAdjustable("SATES21-XSHV166:V-0-S-CH4", internal=True),
#    V_remi             = PVAdjustable("SATES21-XSHV166:V-0-S-CH7", internal=True),
#    V_remi             = PVAdjustable("SATES21-XSHV166:V-1-S-CH4", internal=True),
#    V_remi             = PVAdjustable("SATES21-XSHV166:V-1-S-CH6", internal=True),

#    ToFV1m             = PVAdjustable("SATES21-XSHV166:V-1-S-CH13", internal=True),
#    ToFV1p             = PVAdjustable("SATES21-XSHV166:V-0-S-CH13", internal=True),
#    ToFV2m             = PVAdjustable("SATES21-XSHV166:V-1-S-CH14", internal=True),
#    ToFV2p             = PVAdjustable("SATES21-XSHV166:V-0-S-CH14", internal=True),
#    ToFV3m             = PVAdjustable("SATES21-XSHV166:V-1-S-CH15", internal=True),
#    ToFV3p             = PVAdjustable("SATES21-XSHV166:V-0-S-CH15", internal=True),

    VMI_tube             = PVAdjustable("SATES21-XSHV166:V-0-S-CH1", internal=True),
    VMI_add_electrode             = PVAdjustable("SATES21-XSHV166:V-0-S-CH2", internal=True),
    MCP_front             = PVAdjustable("SATES21-XSHV166:V-0-S-CH3", internal=True),
    MCP_front_1100V             = PVAdjustable("SATES21-XSHV166:V-0-S-CH5", internal=True),
    MCP_front_800V             = PVAdjustable("SATES21-XSHV166:V-0-S-CH6", internal=True),


    Phosphor             = PVAdjustable("SATES21-XSHV166:V-1-S-CH0", internal=True),
    MCP_middle             = PVAdjustable("SATES21-XSHV166:V-1-S-CH3", internal=True),
    MCP_back             = PVAdjustable("SATES21-XSHV166:V-1-S-CH4", internal=True),




#    empty="", # just an example!
#    HemisphereEk = "tobeUpdated",
#    HemisphereEp = "tobeUpdated",
#    HemisphereMode="tobeUpdated",
#    HemisphereSlit="tobeUpdated",

#    PhotonSpecX = "tobeUpdated",
#    PhotonSpecY = "tobeUpdated",
#    PhotonSpecdet="tobeUpdated",
#    PhotonSpecOrder = "tobeUpdated",
#    sample="N2",#

#    temp1_channel      = PVAdjustable("SATES20-CWAG-GEP01:TEMP07", internal=True),
#    temp2_channel      = PVAdjustable("SATES20-CWAG-GEP01:TEMP08", internal=True),


#    mic_linear              = PVAdjustable("SATES21-XSMA166:MOT2:MOTRBV", internal=True),
#    mic_paddle_y            = PVAdjustable("SATES21-XSMA166:MOT4:MOTRBV", internal=True),
#    mic_paddle_x            = PVAdjustable("SATES21-XSMA166:MOT5:MOTRBV", internal=True),
#    mic_paddle_z            = PVAdjustable("SATES21-XSMA166:MOT6:MOTRBV", internal=True),
#    zigzag_y                = PVAdjustable("SATES21-XSMA166:MOT7:MOTRBV", internal=True),
#    zigzag_x                = PVAdjustable("SATES21-XSMA166:MOT8:MOTRBV", internal=True),
#    zigzag_z                = PVAdjustable("SATES21-XSMA166:MOT9:MOTRBV", internal=True),
#    slit_downstream_top     = PVAdjustable("SATES21-XSMA166:MOT10:MOTRBV", internal=True),
#    slit_downstream_x       = PVAdjustable("SATES21-XSMA166:MOT11:MOTRBV", internal=True),
#    slit_downstream_bottom  = PVAdjustable("SATES21-XSMA166:MOT12:MOTRBV", internal=True),
#    slit_front_xneg         = PVAdjustable("SATES21-XSMA166:MOT13:MOTRBV", internal=True),
#    slit_front_xpos         = PVAdjustable("SATES21-XSMA166:MOT14:MOTRBV", internal=True),

#    pulse_energy_attgas   = PVStringAdjustable("SATFE10-OGAT053:pulseenergy", internal=True),
    transmission_gasAtt   = PVAdjustable("SATFE10-OGAT053:transmission", internal=True),

#    att64              = PVStringAdjustable("SATFE10-OATT064:MOT2TRANS.VALD", internal=True),
#    att65              = PVStringAdjustable("SATFE10-OATT065:MOT2TRANS.VALD", internal=True),

#    Tablechamber2X             = PVAdjustable("SATES22-XOTA168:W_X.RBV", internal=True),
#    Tablechamber2Y             = PVAdjustable("SATES22-XOTA168:W_Y.RBV", internal=True),
#    Tablechamber2Z             = PVAdjustable("SATES22-XOTA168:W_Z.RBV", internal=True),

    Tablechamber3X             = PVAdjustable("SATES23-XOTA169:W_X.RBV", internal=True),
    Tablechamber3Y             = PVAdjustable("SATES23-XOTA169:W_Y.RBV", internal=True),
    Tablechamber3Z             = PVAdjustable("SATES23-XOTA169:W_Z.RBV", internal=True),

#    Tablechamber4X             = PVAdjustable("SATES24-XOTA171:W_X.RBV", internal=True),
#    Tablechamber4Y             = PVAdjustable("SATES24-XOTA171:W_Y.RBV", internal=True),
#    Tablechamber4Z             = PVAdjustable("SATES24-XOTA171:W_Z.RBV", internal=True),


#    LaserIn            =  PVAdjustable("SLAAT21-LDIO-LAS6291:SET_BO01.DESC", internal=True),


# Holography beamtime
#    sample_stage_x     = PVAdjustable("SATES23-XSMA169:MOT3:MOTRBV", internal=True),
#    sample_stage_y     = PVAdjustable("SATES23-XSMA169:MOT1:MOTRBV", internal=True),
#    sample_stage_z     = PVAdjustable("SATES23-XSMA169:MOT2:MOTRBV", internal=True),
#    beam_block_x     = PVAdjustable("SATES23-XSMA169:MOT7:MOTRBV", internal=True),
#    beam_block_y     = PVAdjustable("SATES23-XSMA169:MOT8:MOTRBV", internal=True),
#    beam_block_z     = PVAdjustable("SATES23-XSMA169:MOT9:MOTRBV", internal=True),
#    slit_x_pos     = PVAdjustable("SATES23-XSMA169:MOT10:MOTRBV", internal=True),
#    slit_y_bottom     = PVAdjustable("SATES23-XSMA169:MOT11:MOTRBV", internal=True),
#    slit_y_top     = PVAdjustable("SATES23-XSMA169:MOT12:MOTRBV", internal=True),
#    slit_x_neg     = PVAdjustable("SATES23-XSMA169:MOT13:MOTRBV", internal=True),
#    beam_block         = PVAdjustable("SATES23-XSMA169:MOT6:MOTRBV", internal=True),
#    pressChamb3        = PVAdjustable("SATES21-VM-VT3010:PRESSURE", internal=True),
#    pressChamb3bis     = PVAdjustable("SATES21-VM-VT3012:PRESSURE", internal=True),

    **undulator_info
)



overview = SimpleDevice("Maloja Overview", **spreadsheet_info)



@as_shortcut
def print_overview():
    print(overview)

@as_shortcut
def print_line_for_spreadsheet():
    ov = overview.__dict__
    def get(i):
        if i in ov:
            return str(ov[i].get())
        return ""

    res = [get(i) for i in spreadsheet_line]
    res = ",".join(res)
    print(res)



