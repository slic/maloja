from datetime import datetime
from time import sleep, time
import numpy as np
from epics import PV
from slic.utils import nice_arange
from slic.devices.general.motor import Motor


#def kescan3D():
#    x0 = 11.0
#    y0 = 4.0
#    z0 = 0.029

#    npulses = 500

#    step = 0.0005

#    xstart = 10.535
#    xstop  = 10.575

#    ystart = 0.295
#    ystop  = 0.335

#    ntry = "04"

#    wait_time = 0.1

#    for i in range(100):
##        for z_val in nice_arange(z0-1.0, z0+0.2, 0.05):
##        for z_val in np.linspace(z0-1.0, z0+0.2, 25):
#        for z_val in list(reversed(list(z0 + np.linspace(-1.0, -0.4, 5)) + list(z0 + np.linspace(-0.2, 0.2, 5)))):
#            mot_z.set(z_val).wait()

#            print(i)

#            print(mot_x)
#            print(mot_y)
#            print(mot_z)

#            mot_y.set(y0).wait()
#            mot_x.set(xstart).wait()
#            scan.scan1D(mot_x, xstart, xstop, step, npulses, f"focus_knifeedge3D_x_scan_{ntry}_{z_val}", return_to_initial_values=False)
#            sleep(wait_time)

#            print(mot_x)
#            print(mot_y)
#            print(mot_z)

#            mot_x.set(x0).wait()
#            mot_y.set(ystart).wait()
#            scan.scan1D(mot_y, ystart, ystop, step, npulses, f"focus_knifeedge3D_y_scan_{ntry}_{z_val}", return_to_initial_values=False)
#            sleep(wait_time)

#            print("-" * 10)
#            print()



#def overnight():
#    current = mot_z.get_current_value()

#    start = time()

#    ntry = 7

#    for i in range(100):
#        si = str(i).zfill(6)

#        fname = f"z_hop_{ntry}_coarse_{si}"
#        print(fname)

#        print(current, mot_z)
#        mot_z.set_target_value(current).wait()
##        sleep(0.5)
#        if np.abs(mot_z.get_current_value() - current) > 0.01:
#            break

#        print(current, mot_z)
#        scan.scan1D(mot_z, -0.1, +0.1, 0.1, 500, fname, relative=True, return_to_initial_values=True)
#        print(current, mot_z)
##        sleep(0.5)

#        fname = f"z_hop_{ntry}_fine_{si}"
#        print(fname)

#        print(current, mot_z)
#        mot_z.set_target_value(current).wait()
##        sleep(0.5)
#        if np.abs(mot_z.get_current_value() - current) > 0.01:
#            break

#        print(current, mot_z)
#        scan.scan1D(mot_z, -0.01, +0.01, 0.001, 500, fname, relative=True, return_to_initial_values=True)
#        print(current, mot_z)
##        sleep(0.5)

#        now = time()
#        deltat = now - start
#        print(start, now, deltat, 4*60*60)
#        if deltat > 4*60*60:
#            break



#def kescan3Dv2():
#    x0 = 23.947
#    y0 = 0

#    npulses = 25 * 100/25

#    step = 0.0005

#    xstart = 21.897
#    xstop  = 21.997

#    ystart = -2.573
#    ystop  = -2.713

#    wait_time = 0.01

#    for i in range(100):
#        for z_val in nice_arange(0.2, -1.3, 0.1):
#            mot_z.set(z_val).wait()

#            print(i)

#            print(mot_x)
#            print(mot_y)
#            print(mot_z)

#            mot_y.set(y0).wait()
#            mot_x.set(xstart).wait()
#            scan.scan1D(mot_x, xstart, xstop, step, npulses, f"focus_knifeedge3D_v2_x_scan_{i}_{z_val}", return_to_initial_values=False)
#            sleep(wait_time)

#            print(mot_x)
#            print(mot_y)
#            print(mot_z)

#            mot_x.set(x0).wait()
#            mot_y.set(ystart).wait()
#            scan.scan1D(mot_y, ystart, ystop, step, npulses, f"focus_knifeedge3D_v2_y_scan_{i}_{z_val}", return_to_initial_values=False)
#            sleep(wait_time)

#            print("-" * 10)
#            print()



#def tof_loop():
#    for i in range(50):
#        fn = f"tof_loop2_{i}"
#        daq.acquire(fn, n_pulses=10*10000)



#def overnight():
#    sig = 101.617
#    bkg = sig + 1
#    while True:
#        delay.set(sig).wait()
#        sleep(10)
#        daq.acquire("TT_overnight_sig", n_pulses=10 * 10000)
#        delay.set(bkg).wait()
#        daq.acquire("TT_overnight_bkg", n_pulses=10 * 1000)
#        sleep(10)



#def overnight():
#    start = 102.1
#    stop  = 102.4
#    step  =   0.015

#    n_pulses = 10 * 500

#    bkg = 103.3

#    for i in range(1000):
#        print(i, "sig")
#        delay.set(start).wait()
#        sleep(10)
#        scan.scan1D(delay, start, stop, step, n_pulses, f"TT_overnight2_sig_{i:04}", return_to_initial_values=False)

#        print(i, "bkg")
#        delay.set(bkg).wait()
#        daq.acquire(f"TT_overnight2_bkg_{i:04}", n_pulses=n_pulses)
#        sleep(10)



#def overnight():
#    start = 102.0
#    stop  = 102.3
#    step  =   0.0075

#    n_pulses = 10 * 500

#    bkg = 103.3

#    for i in range(1000):
#        print(i, "sig")
#        delay.set(start).wait()
#        sleep(10)
#        scan.scan1D(delay, start, stop, step, n_pulses, f"TT_overnight3_sig_{i:04}", return_to_initial_values=False)

#        print(i, "bkg")
#        delay.set(bkg).wait()
#        daq.acquire(f"TT_overnight3_bkg_{i:04}", n_pulses=n_pulses)
#        sleep(10)








#SATFE10-OPSH066:REQUEST

#STATE  0: close
#STATE  1: open



#def overnight():
#    shutter = PV("SATOP21-OPSH138:REQUEST")

#    print("open shutter")
#    shutter.put(1)
#    sleep(3)

#    base = "TT_overnight"

#    x0 = 22.24
#    start = x0 - 0.23
#    stop  = x0 + 0.23
#    step  = 0.01

#    n_pulses = 2 * 2000

#    for i in range(1000):
#        print(i, "sig")
#        scan.scan1D(exp_delay, start, stop, step, n_pulses, f"{base}_sig_{i:04}", return_to_initial_values=True)

#        print("close shutter")
#        shutter.put(0)
#        sleep(3)

#        print(i, "bkg")
#        daq.acquire(f"{base}_bkg_{i:04}", n_pulses=n_pulses)

#        print("open shutter")
#        shutter.put(1)
#        sleep(3)





#def overnight():
#    shutter = PV("SATOP21-OPSH138:REQUEST")

#    print("open shutter")
#    shutter.put(1)
#    sleep(3)

#    base = "TT_overnight2"

#    x0 = 21.36
#    start = x0 - 0.225
#    stop  = x0 + 0.225
#    step  = 0.003

#    n_pulses = 2 * 1000

#    for i in range(1000):
#        print(i, "sig")
#        scan.scan1D(exp_delay, start, stop, step, n_pulses, f"{base}_sig_{i:04}", return_to_initial_values=True)

#        print("close shutter")
#        shutter.put(0)
#        sleep(3)

#        print(i, "bkg")
#        daq.acquire(f"{base}_bkg_{i:04}", n_pulses=n_pulses)

#        print("open shutter")
#        shutter.put(1)
#        sleep(3)





#def overnight():
#    shutter = PV("SATOP21-OPSH138:REQUEST")

#    print("open shutter")
#    shutter.put(1)
#    sleep(3)

#    base = "TT_overnight3"

#    x0 = 16.92
#    start = x0 - 0.15
#    stop  = x0 + 0.15
#    step  = 0.003

#    n_pulses = 4 * 500

#    for i in range(1000):
#        print(i, "sig")
#        scan.scan1D(exp_delay, start, stop, step, n_pulses, f"{base}_sig_{i:04}", return_to_initial_values=True)

#        print("close shutter")
#        shutter.put(0)
#        sleep(3)

#        print(i, "bkg")
#        daq.acquire(f"{base}_bkg_{i:04}", n_pulses=n_pulses)

#        print("open shutter")
#        shutter.put(1)
#        sleep(3)








#end_time = datetime(2021, 9, 3, 8, 0, 0, 0)


#def overnight():
#    shutter = PV("SATOP21-OPSH138:REQUEST")
#
#    print("open shutter")
#    shutter.put(1)
#    sleep(3)
#
#    base = "TT_nitrogen_overnight2"
#
#    x0 = -14.55
#    start = x0 - 0.2
#    stop  = x0 + 0.2
#    step  = 0.01
#
#    n_pulses = 40 * 100
#
#    for i in range(1000):
#        if datetime.now() > end_time:
#            break
#
#        print(i, "sig")
#        scan.scan1D(exp_delay, start, stop, step, n_pulses, f"{base}_sig_{i:04}", return_to_initial_values=True)
#
#        print("close shutter")
#        shutter.put(0)
#        sleep(3)
#
#        print(i, "bkg")
#        daq.acquire(f"{base}_bkg_{i:04}", n_pulses=n_pulses)
#
#        print("open shutter")
#        shutter.put(1)
#        sleep(3)
#
#
#    shutter2 = PV("SATFE10-OPSH066:REQUEST")
#
#    print("close both shutters")
#    shutter.put(0)
#    shutter2.put(0)
#    sleep(3)



#def overnight():
#    shutter = PV("SATOP21-OPSH138:REQUEST")

#    print("open shutter")
#    shutter.put(1)
#    sleep(3)

#    base = "CH3I_C1s_with_shutter_200_microJ"

#    x0 = -2.816
#    start = x0 - 0.12
#    stop  = x0 + 0.12
#    step  = 0.003

#    n_pulses = 4 * 250

#    for i in range(1000):
#        print(i, "sig")
#        scan.scan1D(exp_delay, start, stop, step, n_pulses, f"{base}_sig_{i:04}", return_to_initial_values=True)

#        print("close shutter")
#        shutter.put(0)
#        sleep(3)

#        print(i, "bkg")
#        daq.acquire(f"{base}_bkg_{i:04}", n_pulses=n_pulses)

#        print("open shutter")
#        shutter.put(1)
#        sleep(3)




#def overnight():
#     currents = [
#        1.72090994e-04, 1.19335371e+01, 1.68765438e+01, 2.06694283e+01,
#        2.38669628e+01, 2.66840341e+01, 2.92308494e+01, 3.15728768e+01,
#        3.37527725e+01, 3.58001658e+01, 3.77366295e+01, 3.95784494e+01,
#        4.13382779e+01, 4.30261768e+01, 4.46503044e+01, 4.62173840e+01,
#        4.77330347e+01, 4.92020095e+01, 5.06283713e+01, 5.20156260e+01,
#        5.33668232e+01, 5.46846359e+01, 5.59714223e+01, 5.72292751e+01,
#        5.84600621e+01, 5.96654582e+01, 6.08469725e+01, 6.20059702e+01,
#        6.31436911e+01, 6.42612654e+01, 6.53597265e+01, 6.64400224e+01,
#        6.75030253e+01, 6.85495396e+01, 6.95803094e+01, 7.05960243e+01,
#        7.15973250e+01, 7.25848080e+01, 7.35590300e+01, 7.45205108e+01,
#        7.54697376e+01, 7.64071670e+01, 7.73332280e+01, 7.82483243e+01,
#        7.91528361e+01, 8.00471224e+01, 8.09315219e+01, 8.18063555e+01,
#        8.26719268e+01, 8.35285237e+01, 8.43764195e+01
#    ]

#    base = "CH3I_C1s_with_shutter_200_microJ"

#    x0 = -2.816
#    start = x0 - 0.12
#    stop  = x0 + 0.12
#    step  = 0.003

#    n_pulses = 4 * 250

#    for i in range(currents):
#        chic_delay.set(I).wait()

#        print("Chicane set to :",i)

#        for ene in range(535,544):
#            scan.scan1D(exp_delay, start, stop, step, n_pulses, f"{base}_sig_{i:04}", return_to_initial_values=True)

#        print("close shutter")
#        shutter.put(0)
#        sleep(3)

#        print(i, "bkg")
#        daq.acquire(f"{base}_bkg_{i:04}", n_pulses=n_pulses)

#        print("open shutter")
#        shutter.put(1)
#        sleep(3)








#def overnight():
#    shutter = PV("SATOP21-OPSH138:REQUEST")

#    print("open shutter")
#    shutter.put(1)
#    sleep(3)

#    #base = "I4d_high_resolution_Ek472_Ep100_TTincluded_slit_2_008"
#    #base = "I4d_high_resolution_Ek472_Ep200_TTincluded_slit_2_010"

#    base = "CF3I_C1s_Ek230_Ep200_TTincluded_slit2_2609_130microJ_002"

#    x0 = 15.813

#    start = x0 - 0.12
#    stop  = x0 + 0.4

#    step  = 0.004

#    n_pulses = 2 * 300

#    for i in range(1000):
#        print(i, "sig")
#        scan.scan1D(exp_delay, start, stop, step, n_pulses, f"{base}_sig_{i:04}", return_to_initial_values=True)

#        print("close shutter")
#        shutter.put(0)
#        sleep(3)

#        print(i, "bkg")
#        daq.acquire(f"{base}_bkg_{i:04}", n_pulses=n_pulses)

#        print("open shutter")
#        shutter.put(1)
#        sleep(3)





#from datetime import datetime
#from time import sleep, time

#end_time = datetime(2021, 10, 14, 6, 0, 0, 0)

#def megadimensionalscan():
#    positions = [
#        1.72090994e-04, 1.19335371e+01, 1.68765438e+01, 2.06694283e+01,
#        2.38669628e+01, 2.66840341e+01, 2.92308494e+01, 3.15728768e+01,
#        3.37527725e+01, 3.58001658e+01, 3.77366295e+01, 3.95784494e+01,
#        4.13382779e+01, 4.30261768e+01, 4.46503044e+01, 4.62173840e+01,
#        4.77330347e+01, 4.92020095e+01, 5.06283713e+01, 5.20156260e+01,
#        5.33668232e+01, 5.46846359e+01, 5.59714223e+01, 5.72292751e+01,
#        5.84600621e+01, 5.96654582e+01, 6.08469725e+01, 6.20059702e+01,
#        6.31436911e+01, 6.42612654e+01, 6.53597265e+01, 6.64400224e+01,
#        6.75030253e+01, 6.85495396e+01, 6.95803094e+01, 7.05960243e+01,
#        7.15973250e+01, 7.25848080e+01, 7.35590300e+01, 7.45205108e+01,
#        7.54697376e+01, 7.64071670e+01, 7.73332280e+01, 7.82483243e+01,
#        7.91528361e+01, 8.00471224e+01, 8.09315219e+01, 8.18063555e+01,
#        8.26719268e+01, 8.35285237e+01, 8.43764195e+01
#    ]

##    cycle = PV("SATUN14-MBND100:CYCLE")

#    n_pulses = 2000

#    und2.set(545)

##    for i in range(1000):
##        if datetime.now() > end_time:
##            break

#    for energy in (402, 420):
#        und1.set(energy)

#        fname = f"overnight_{energy}eV" #_{i:04}"
#        scan.scan1D(chic_delay, positions, n_pulses, fname)
##        cycle.put(1, wait=True)
##        sleep(300)



#def overnight():
#    currents = [
#        1.72090994e-04, 1.19335371e+01, 1.68765438e+01, 2.06694283e+01,
#        2.38669628e+01, 2.66840341e+01, 2.92308494e+01, 3.15728768e+01,
#        3.37527725e+01, 3.58001658e+01, 3.77366295e+01, 3.95784494e+01,
#        4.13382779e+01, 4.30261768e+01, 4.46503044e+01, 4.62173840e+01,
#        4.77330347e+01, 4.92020095e+01, 5.06283713e+01, 5.20156260e+01,
#        5.33668232e+01, 5.46846359e+01, 5.59714223e+01, 5.72292751e+01,
#        5.84600621e+01, 5.96654582e+01, 6.08469725e+01, 6.20059702e+01,
#        6.31436911e+01, 6.42612654e+01, 6.53597265e+01, 6.64400224e+01,
#        6.75030253e+01, 6.85495396e+01, 6.95803094e+01, 7.05960243e+01,
#        7.15973250e+01, 7.25848080e+01, 7.35590300e+01, 7.45205108e+01,
#        7.54697376e+01, 7.64071670e+01, 7.73332280e+01, 7.82483243e+01,
#        7.91528361e+01, 8.00471224e+01, 8.09315219e+01, 8.18063555e+01,
#        8.26719268e+01, 8.35285237e+01, 8.43764195e+01
#    ]

##    und2.set(545).wait()
##        for energy in (402, 420):

#    for I in currents:
#        chic_delay.set(I).wait()

#        for energy1 in (401.5, 406.5, 420):
#            und1.set(energy1).wait()

#            for energy2 in (534, 545):
#                und2.set(energy2).wait()

#                fname = f"overnight4_{energy1}eV_{energy2}eV_{I}A"
#                print(fname)
#                daq.acquire(fname, n_pulses=1500*4).wait()









#def delay_energy_scan():
##    currents = [
##        1.72090994e-04, 1.19335371e+01, 1.68765438e+01, 2.06694283e+01,
##        2.38669628e+01, 2.66840341e+01, 2.92308494e+01, 3.15728768e+01,
##        3.37527725e+01, 3.58001658e+01, 3.77366295e+01, 3.95784494e+01,
##        4.13382779e+01, 4.30261768e+01, 4.46503044e+01, 4.62173840e+01,
##        4.77330347e+01, 4.92020095e+01, 5.06283713e+01, 5.20156260e+01,
##        5.33668232e+01, 5.46846359e+01, 5.59714223e+01, 5.72292751e+01,
##        5.84600621e+01, 5.96654582e+01, 6.08469725e+01, 6.20059702e+01,
##        6.31436911e+01, 6.42612654e+01, 6.53597265e+01, 6.64400224e+01,
##        6.75030253e+01, 6.85495396e+01, 6.95803094e+01, 7.05960243e+01,
###        7.15973250e+01, 7.25848080e+01, 7.35590300e+01, 7.45205108e+01,
###        7.54697376e+01, 7.64071670e+01, 7.73332280e+01, 7.82483243e+01,
###        7.91528361e+01, 8.00471224e+01, 8.09315219e+01, 8.18063555e+01,
###        8.26719268e+01, 8.35285237e+01, 8.43764195e+01
##    ]

##    currents = [
##        20, 35, 50
##    ]

#    currents = [
#        20.6694283,
#        23.8669628,
#        26.6840341,
#        29.2308494,
#        31.5728768,
#        33.7527725,
#        35.8001658,
#        37.7366295,
#        39.5784494,
#        41.3382779,
#        43.0261768,
#        44.6503044,
#        46.217384]
#        #47.7330347,
#        #49.2020095,
#        #50.6283713]
#        #52.015626,
#        #53.3668232,
#        #54.6846359,
#        #55.9714223,
#        #57.2292751,
#        #59.6654582,
#        #62.0059702,
#        #64.2612654,
#        #66.4400224,
#        #68.5495396,
#        #70.5960243
#    #]


##    shutter1 = PV("SATFE10-OPSH066:REQUEST")
##    shutter2 = PV("SATOP21-OPSH138:REQUEST")
#    cycle = PV("SATUN14-MBND100:CYCLE")

##    end_time = datetime(2021, 11, 22, 8)

#    for i in range(1):
##        now = datetime.now()
##        if now > end_time:
##            break

#        cycle.put(1, wait=True)
#        sleep(250)

#        for I in currents:
#            chic_delay.set(I).wait()

##            for energy1 in (510, 537.8, 555): # O2
##            for energy1 in (540, 560): # N2O
##            for energy1 in (403, 406.8, 420):
#            for energy1 in (403,):
#                und1.set(energy1).wait()

##                for energy2 in (520, 543, 565): # O2
##                for energy2 in (525, 535, 555): # N2O
##                for energy2 in (543, 560):
#                for energy2 in range(534, 544+1):
#                    und2.set(energy2).wait()


## !!! !!! !!! Change name here:
#                    fname = f"morning2_N2O_22mbar_{i:04}_{energy1}eV_{energy2}eV_{I}A"
#                    print(fname)

##                    while True:
##                        check_intensity.get_ready()
##                        daq.acquire(fname, n_pulses=1500*2).wait()
##                        if check_intensity.is_happy():
##                            break

#                    while check_intensity.wants_repeat():
#                        daq.acquire(fname, n_pulses=1000 * 2).wait()


##    print("close shutter")
##    shutter1.put(0)
##    shutter2.put(0)



















#def overnight():
#    currents = [
#        1.72090994e-04, 1.19335371e+01, 1.68765438e+01, 2.06694283e+01,
#        2.38669628e+01, 2.66840341e+01, 2.92308494e+01, 3.15728768e+01,
#        3.37527725e+01, 3.58001658e+01, 3.77366295e+01, 3.95784494e+01,
#        4.13382779e+01, 4.30261768e+01, 4.46503044e+01, 4.62173840e+01,
#        4.77330347e+01, 4.92020095e+01, 5.06283713e+01, 5.20156260e+01,
#        5.33668232e+01, 5.46846359e+01, 5.59714223e+01, 5.72292751e+01,
#        5.84600621e+01, 5.96654582e+01, 6.08469725e+01, 6.20059702e+01,
#        6.31436911e+01, 6.42612654e+01, 6.53597265e+01, 6.64400224e+01,
#        6.75030253e+01, 6.85495396e+01, 6.95803094e+01, 7.05960243e+01,
##        7.15973250e+01, 7.25848080e+01, 7.35590300e+01, 7.45205108e+01,
##        7.54697376e+01, 7.64071670e+01, 7.73332280e+01, 7.82483243e+01,
##        7.91528361e+01, 8.00471224e+01, 8.09315219e+01, 8.18063555e+01,
##        8.26719268e+01, 8.35285237e+01, 8.43764195e+01
#    ]

##    currents = [
##        20, 35, 50
##    ]

##    currents = [
##        20.6694283,
##        23.8669628,
##        26.6840341,
##        29.2308494,
##        31.5728768,
##        33.7527725,
##        35.8001658,
##        37.7366295,
##        39.5784494,
##        41.3382779,
##        43.0261768,
##        44.6503044,
##        46.217384]
##        #47.7330347,
##        #49.2020095,
##        #50.6283713]
##        #52.015626,
##        #53.3668232,
##        #54.6846359,
##        #55.9714223,
##        #57.2292751,
##        #59.6654582,
##        #62.0059702,
##        #64.2612654,
##        #66.4400224,
##        #68.5495396,
##        #70.5960243
##    #]


##    shutter1 = PV("SATFE10-OPSH066:REQUEST")
##    shutter2 = PV("SATOP21-OPSH138:REQUEST")
#    cycle = PV("SATUN14-MBND100:CYCLE")

##    end_time = datetime(2021, 11, 22, 8)

#    for i in range(100):
##        now = datetime.now()
##        if now > end_time:
##            break

#        cycle.put(1, wait=True)
#        sleep(250)

#        for I in currents:
#            chic_delay.set(I).wait()

##            for energy1 in (510, 537.8, 555): # O2
##            for energy1 in (540, 560): # N2O
##            for energy1 in (403, 406.8, 420):
##            for energy1 in (403,):
#            for energy1 in (403, 406, 420):
#                und1.set(energy1).wait()

##                for energy2 in (520, 543, 565): # O2
##                for energy2 in (525, 535, 555): # N2O
##                for energy2 in (543, 560):
##                for energy2 in range(534, 544+1):
#                for energy2 in (543, 560):
#                    und2.set(energy2).wait()

#                    fname = f"overnight7_{i:04}_{energy1}eV_{energy2}eV_{I}A"
#                    print(fname)

##                    while True:
##                        check_intensity.get_ready()
##                        daq.acquire(fname, n_pulses=1500*2).wait()
##                        if check_intensity.is_happy():
##                            break

#                    while check_intensity.wants_repeat():
#                        daq.acquire(fname, n_pulses=1000 * 2).wait()


##    print("close shutter")
##    shutter1.put(0)
##    shutter2.put(0)





#def overnight():
##    currents = [
###        10.0,
###        16.0,
###        22.7,
###        27.7,
###        32.0,
###        34.0,
###        35.8,
###        37.6,
###        39.2,
###        40.8,
###        42.2,
###        45.3,
###        48.0,
###        53.1
###        57.7,
###        62.0,
###        71.6,
###        80.0
###
##         31.48930094022534,
##         32.48918407498299,
##         33.459197405725845,
##         34.40186769985522,
##         35.31938437212558,
##         36.21365936509662,
##         37.08637401803952,
##         37.939016223878696,
##         38.77291022432906,
##         39.589240754262065,
##         40.38907278889524,
##         41.17336783651211,
##         41.9429974880303
###        35.8,
###        36.7,
###        37.6,
###        38.4,
###        39.2,
###        40.0,
###        40.8,
###        42.3
##    ]

#    currents = [
#        1.72090994e-04, 1.19335371e+01, 1.68765438e+01, 2.06694283e+01,
#        2.38669628e+01, 2.66840341e+01, 2.92308494e+01, 3.15728768e+01,
#        3.37527725e+01, 3.58001658e+01, 3.77366295e+01, 3.95784494e+01,
#        4.13382779e+01, 4.30261768e+01, 4.46503044e+01, 4.62173840e+01,
#        4.77330347e+01, 4.92020095e+01, 5.06283713e+01, 5.20156260e+01,
#        5.33668232e+01, 5.46846359e+01, 5.59714223e+01, 5.72292751e+01,
#        5.84600621e+01, 5.96654582e+01, 6.08469725e+01, 6.20059702e+01,
#        6.31436911e+01, 6.42612654e+01, 6.53597265e+01, 6.64400224e+01,
#        6.75030253e+01, 6.85495396e+01, 6.95803094e+01, 7.05960243e+01,
##        7.15973250e+01, 7.25848080e+01, 7.35590300e+01, 7.45205108e+01,
##        7.54697376e+01, 7.64071670e+01, 7.73332280e+01, 7.82483243e+01,
##        7.91528361e+01, 8.00471224e+01, 8.09315219e+01, 8.18063555e+01,
##        8.26719268e+01, 8.35285237e+01, 8.43764195e+01
#        7.45205108e+01,
#        7.82483243e+01,
#        8.18063555e+01,
#    ]

#    for i in range(100):

#        for I in currents:
#            chic_delay.set(I).wait()

#            for energy1 in (395, 403, 406, 420):
#                und1.set(energy1).wait()

#                for energy2 in (543,):
#                    und2.set(energy2).wait()

#                    fname = f"overnight10_{i:04}_{energy1}eV_{energy2}eV_{I}A"
#                    print(fname)

#                    while check_intensity.wants_repeat():
#                        daq.acquire(fname, n_pulses=1000 * 2).wait()

#        cycle_magnet().wait()






#def morning():
## this for the signal
#    currents = [
#         10.0,
##        28.83830235412312,
##        30.977257705828176,
#        31.99314930809352,
##        32.977757827608926,
##        33.93380646207095,
##        34.86364484244994,
##        35.76931706628362,
##        36.652614587304065,
##        37.51511788345494,
##        38.358229684315006,
#        39.18320176115609,
##        39.99115673481851,
##        40.78310599684759,
##        41.55996455126603,
##        43.071659987530104,
#        43.80,
##        44.54,
#        50.5,
#        62.0,
#        80.0,
#       100.0
#    ]

### this for the reference
##    currents = [
##        30.977257705828176,
##        39.18320176115609,
##        41.55996455126603,
##    ]

#    # change number here, in case of repeat
#    for i in [0]:
#        cycle_magnet().wait()

#        for I in currents:
#            chic_delay.set(I).wait()

#            for energy1 in (406,): # if done, switch to: 403
#                und1.set(energy1).wait()

#                for energy2 in range(530, 545+1):
#                    und2.set(energy2).wait()

#                    # adjust name here (5 should be the next correct number)
#                    fname = f"afternoon16_N2O_{i:04}_{energy1}eV_{energy2}eV_{I}A"
#                    print(fname)

#                    while check_intensity.wants_repeat():
#                        daq.acquire(fname, n_pulses=1000 * 2).wait()





#def mourning():
#    currents = [
#        10.0,
#        31.99314930809352,
#        39.18320176115609,
#        43.80,
#        50.5,
#        62.0,
#        80.0,
#        100.0
#    ]
#
#    end_time = datetime(2021, 11, 29, 7, 45)
#
#    for i in range(100):
#        now = datetime.now()
#        if now > end_time:
#            break
#
#        for I in currents:
#            chic_delay.set(I).wait()
#
#            for energy1 in (406,): # if done, switch to: 403
#                und1.set(energy1).wait()
#
#                for energy2 in range(530, 545+1):
#                    und2.set(energy2).wait()
#
#                    fname = f"afternoon17_N2O_{i:04}_{energy1}eV_{energy2}eV_{I}A"
#                    print(datetime.now(), fname)
#
#                    while check_intensity.wants_repeat():
#                        daq.acquire(fname, n_pulses=1000 * 2).wait()
#
#        cycle_magnet().wait()
#
#
#
#def klappe_zu():
#    shutter1 = PV("SATFE10-OPSH066:REQUEST")
#    shutter2 = PV("SATOP21-OPSH138:REQUEST")
#
#    end_time = datetime(2021, 11, 29, 8)
#
#    while True:
#        now = datetime.now()
#        if now > end_time:
#            break
#        print(now)
#        sleep(30)
#
#    print("close shutters", datetime.now())
#    shutter1.put(0)
#    shutter2.put(0)




# def TTmeasurements(daq, numIterations, nBackground, nMeas):
#     #increment run number
#     print(daq.pgroup)
#     repratefactor = 10 #check reprate of machine

#     shutter266 = PV('SLAAT21-LDIO-LAS6291:SET_BO01')
#     exp_delay = Motor("SLAAT21-LMOT-M704:MOT", name="Laser Exp delay")

#     dsStart = exp_delay.get()

#     ### take background Time tool:
#     for i in range(0,numIterations):

# #        #close shutter
# #        shutter266.put(1)
# #        ### Take data
# #        #timestamp = datetime.today().strftime("%d_%m_%H_%M")
# #        fname = f"TimeToolBackground_{i}"
# #        daq.acquire(fname, n_pulses=nBackground * repratefactor)
# #        #Open shutter
# #        shutter266.put(0)


#         ### take spectrometer bacground Time tool:
#         #while shutters open
#         #move away from T0
#         exp_delay.set(dsStart-1)
#         sleep(3)
#         ### Take data
#         #timestamp = datetime.today().strftime("%d_%m_%H_%M")
#         fname = f"SpectrometerBackground_{i}"
#         daq.acquire(fname, n_pulses=nBackground * repratefactor)
#         #move back to T0
#         exp_delay.set(dsStart)
#         sleep(3)
#         ### measure:

#         #timestamp = datetime.today().strftime("%d_%m_%H_%M")
#         fname = f"Measurement_{i}"
#         daq.acquire(fname, n_pulses=nMeas * repratefactor)

#        n_pulses=nMeas * repratefactor
#        scan.scan1D(exp_delay, start, stop, step, n_pulses, fname, return_to_initial_values=True)


# def TTmeasurementsScan(daq, scan, sStage, numIterations, nBackground, nMeas, start, stop, step):
#     #increment run number
#     print(daq.pgroup)
#     repratefactor = 10 #check reprate of machine
#     print(sStage.name)
#     print(sStage.get())

#     shutter266 = PV('SLAAT21-LDIO-LAS6291:SET_BO01')
# #    delayStage = PV('SLAAT21-LMOT-M704:MOT.VAL')
#     delay = Motor("SLAAT21-LMOT-M704:MOT", name="Laser Exp delay")

#     dsStart = delay.get()

#     ### take background Time tool:
#     for i in range(0,numIterations):

# #        #close shutter
# #        shutter266.put(1)
# #        ### Take data
# #        #timestamp = datetime.today().strftime("%d_%m_%H_%M")
# #        fname = f"TimeToolBackground_{i}"
# #        daq.acquire(fname, n_pulses=nBackground * repratefactor)
# #        #Open shutter
# #        shutter266.put(0)


#         ### take spectrometer bacground Time tool:
#         #while shutters open
#         #move away from T0
#         delay.set(dsStart-1)
#         sleep(3)
#         ### Take data
#         #timestamp = datetime.today().strftime("%d_%m_%H_%M")
#         fname = f"SpectrometerBackground_{i}"
#         daq.acquire(fname, n_pulses=nBackground * repratefactor)
#         #move back to T0
#         delay.set(dsStart)
#         sleep(3)

#         ### measure:

#         #timestamp = datetime.today().strftime("%d_%m_%H_%M")
#         fname = f"Measurement_{i}"
# #        daq.acquire(fname, n_pulses=nMeas * repratefactor)

#         n_pulses=nMeas * repratefactor

#         print(sStage.get())
#         scan.scan1D(sStage, start, stop, step, n_pulses, fname, return_to_initial_values=True, relative=True)



# from tqdm import trange

# def focus_test(daq):
#     daq.pgroup = "p19743"
# #    daq.pgroup = "p19509"
#     i = 1
#     while True:
#         i += 1
#         name = f"focus_test2_{i}"
#         daq.acquire(name, n_pulses=1000)
#         print("sleep")
#         for _ in trange(900):
#             sleep(1)





#def scan_with_bkg():
#    start = 102.1
#    stop  = 102.4
#    step  =   0.015

#    n_pulses = 1 * 1000

#    bkg = 0

#    for i in range(1000):
#        print(i, "sig")
#        delay.set(start).wait()
#        sleep(10)
#        scan.scan1D(delay, start, stop, step, n_pulses, f"TT_overnight2_sig_{i:04}", return_to_initial_values=False)

#        print(i, "bkg")
#        delay.set(bkg).wait()
#        daq.acquire(f"TT_overnight2_bkg_{i:04}", n_pulses=n_pulses)
#        sleep(10)





# from devices.simone import Magnet, Frequency

# from itertools import count
# from time import sleep

# magnet = Magnet()
# freq = Frequency()
# shutter = Shutter("SATOP21-OPSH138")

# def overnight():

#     pos_ref = -0.30145 #-0.857
#     pos_sig = -0.9516

#     for ii in range(1,4):

#         freq.set(ii)

#         frequency = 6.2832+0.1428*ii #6.2832 #GHz

#         delays1 = [ 0, 5/8,  1/8, 7/8]
#         delays2 = [3/8,  2/8, 6/8, 4/8]

#         n_pulses = 1000
#         Emin, Emax, Edelta = 708, 720, 2

#         for i in range(12):
#         #for i in count():
#             shutter.close()
#             mot3_x.set(pos_ref)
#             sleep(1)
#             shutter.open()
#             sleep(1)

#             magnet.set(0)
#             print("delay:", magnet)
#             scan.scan1D(und, Emin, Emax, Edelta, n_pulses, f"evening02_ref_{frequency}_{i:04}", return_to_initial_values=False)

#             shutter.close()
#             mot3_x.set(pos_sig)
#             sleep(1)
#             shutter.open()

#             for delay in delays1:
#                 magnet.set(delay/frequency*1000)
#                 print("delay:", magnet)
#                 scan.scan1D(und, Emin, Emax, Edelta, n_pulses, f"evening02_sig_{frequency}_{i:04}_{delay}", return_to_initial_values=False)
#             delays1, delays2 = delays2, delays1

#         #shutter.open()




# def smooth_attenuator():
#     photon_energies = [525, 600]
#     iterations = range(24)

#     for p in photon_energies:
#         und.set(p).wait()
#         und.set(p).wait()
#         for i in iterations:
#             name = f"n2o_{p}eV_{i:02}"
#             print(name)
#             daq.acquire(name, n_pulses=60000)

#     print("close shutter")
#     shutter.close()






# def fire():
#     cycle = PV("SATUN14-MBND100:CYCLE")

#     n_acqs = 30
#     n_pulses = 6000
# #    currents = [19.0, 20.6, 22.1, 23.5, 26.1, 27.3, 28.4, 29.5, 30.6, 31.6, 35.4, 38.9, 43.5, 50.0]
#     currents = [10.2, 19, 22, 25, 27.3, 29.5, 35.4, 43.5, 66.7, 50.3]

#    # for E in [660,640]:
#    #    print("energy:", E)
#    #     t = und1.set(E)

# #    cycle.put(1, wait=True)
# #    sleep(250)

# #    t.wait()

#     for I in currents:
#         print("current:", I)
#         chic_delay.set(I).wait()

#         for i in range(n_acqs):
#             print("rep:", i)
#             fname = f"overnight3_{I}A"
#             daq.acquire(fname, n_pulses=n_pulses*2).wait()

# def fire2():
#     #cycle = PV("SATUN14-MBND100:CYCLE")

#     n_acqs = 30
#     n_pulses = 6000
# #    currents = [19.0, 20.6, 22.1, 23.5, 26.1, 27.3, 28.4, 29.5, 30.6, 31.6, 35.4, 38.9, 43.5, 50.0]
#     currents = [22, 25, 27.3, 29.5, 35.4, 43.5, 50.3, 66.7]

#     for E in [640]:
#         print("energy:", E)
#         t = und1.set(E)

#         #cycle.put(1, wait=True)
#         #sleep(250)

#         #t.wait()

#         for I in currents:
#             print("current:", I)
#             chic_delay.set(I).wait()

#             for i in range(n_acqs):
#                 print("rep:", i)
#                 fname = f"overnight2_{E}eV_{I}A"
#                 daq.acquire(fname, n_pulses=n_pulses*2).wait()










def XPS_overnight(daq):
   currents = [
       1.72090994e-04, 1.19335371e+01, 1.68765438e+01, 2.06694283e+01,
       2.38669628e+01, 2.66840341e+01, 2.92308494e+01, 3.15728768e+01,
       3.37527725e+01, 3.58001658e+01, 3.77366295e+01, 3.95784494e+01,
       4.13382779e+01, 4.30261768e+01, 4.46503044e+01, 4.62173840e+01,
       4.77330347e+01, 4.92020095e+01, 5.06283713e+01, 5.20156260e+01,
       5.33668232e+01, 5.46846359e+01, 5.59714223e+01, 5.72292751e+01,
       5.84600621e+01, 5.96654582e+01, 6.08469725e+01, 6.20059702e+01,
       6.31436911e+01, 6.42612654e+01, 6.53597265e+01, 6.64400224e+01,
       6.75030253e+01, 6.85495396e+01, 6.95803094e+01, 7.05960243e+01,
       7.15973250e+01, 7.25848080e+01, 7.35590300e+01, 7.45205108e+01,
       7.54697376e+01, 7.64071670e+01, 7.73332280e+01, 7.82483243e+01,
       7.91528361e+01, 8.00471224e+01, 8.09315219e+01, 8.18063555e+01,
       8.26719268e+01, 8.35285237e+01, 8.43764195e+01
   ]

#    shutter1 = PV("SATFE10-OPSH066:REQUEST")
#    shutter2 = PV("SATOP21-OPSH138:REQUEST")
   cycle = PV("SATUN14-MBND100:CYCLE")
   chic_delay = PV("SATUN14-MBND100:I-SET")
#    print('cycle')
#    end_time = datetime(2021, 11, 22, 8)

   for i in range(30):
#        now = datetime.now()
#        if now > end_time:
#            break

       cycle.put(1, wait=True)
       sleep(250)

       for s,I in enumerate(currents):
            chic_delay.put(I)
            sleep(2)
            fname = f"test_Glycine_{i:04}_{s}_steps_current"
            print(fname)
            daq.acquire(fname, n_pulses=1000 * 5).wait()


#    print("close shutter")
#    shutter1.put(0)
#    shutter2.put(0)











def overnight():
    currents = [
        5,
        11.9335371,
        16.8765438,
        20.6694283,
        23.8669628,
        26.6840341,
        29.2308494,
        31.5728768,
        33.7527725,
        35.8001658,
        37.7366295,
        39.5784494,
        41.3382779,
        43.0261768,
        44.6503044,
        46.217384,
        47.7330347,
        49.2020095,
        50.6283713,
        52.015626,
        55.97142227,
        56.63693345,
        57.29471271,
        57.94502337,
        58.58811413,
        59.22422017,
        59.8535642,
        60.47635732,
        61.09279988,
        61.7030822,
        62.30738531,
        62.90588152,
        63.49873503,
        64.08610243,

        53.5,
        54.8,

        64.66813322,
        65.24497022,
        65.81674998,
        66.38360318,
        66.94565497,
        67.50302528,
        71.5,
        75.8,
        80,
        84,
        87.6
    ]

    n_repeats = 2
    currents = sorted(sorted(set(currents)) * n_repeats)

    energies1 = (694, 725)
    energy2 = 399

    und2.set(energy2).wait()

    for i in range(5):

        for I in currents:
            chic_delay.set(I).wait()

            for energy1 in energies1:
                und1.set(energy1).wait()

                fname = f"overnight02s_{i:02}_{energy1}eV_{energy2}eV_{I}A"
                print(fname)

                while check_intensity.wants_repeat():
                    daq.acquire(fname, n_pulses=5000).wait()

        cycle_magnet().wait()














def overnight():
    currents = [
        5.0,
        11.9335371,
        16.8765438,
        20.6694283,
        23.8669628,
        26.6840341,
        29.2308494,
        31.5728768,
        33.7527725,
        35.8001658,
        37.7366295,
        39.5784494,
        41.3382779,
        43.0261768,
        44.6503044,
        46.217384,
        47.7330347,
        49.2020095,
        50.6283713,
        52.015626,
        53.5,
        54.8,
        55.97142227,
        56.63693345,
        57.29471271,
        57.94502337,
        58.58811413,
        59.22422017,
        59.8535642,
        60.47635732,
        61.09279988,
        61.7030822,
        62.30738531,
        62.90588152,
        63.49873503,
        64.08610243,
        64.66813322,
        65.24497022,
        65.81674998,
        66.38360318,
        66.94565497,
        67.50302528,
        71.5,
        75.8,
        80.0,
        84.0,
        87.6
    ]

    n_repeats = 2
    currents = sorted(sorted(set(currents)) * n_repeats)

    energy1 = 720
    energy2 = 399

#    und1.set(energy1).wait()
#    und2.set(energy2).wait()

    for i in range(100):

        for I in currents:
            chic_delay.set(I).wait()

            printable_I = str(round(I, 1)).replace(".", "_")

            fname = f"overnight03_{i:02}_{energy1}eV_{energy2}eV_{printable_I}A"
            print(fname)

            while check_intensity.wants_repeat():
                daq.acquire(fname, n_pulses=5000).wait()

        cycle_magnet().wait()







def overnight():
    currents = [
        5.0,
        11.9335371,
        16.8765438,
        20.6694283,
        23.8669628,
        26.6840341,
        29.2308494,
        31.5728768,
        33.7527725,
        35.8001658,
        37.7366295,
        39.5784494,
        41.3382779,
        43.0261768,
        44.6503044,
        46.217384,
        47.7330347,
        49.2020095,
        50.6283713,
        52.015626,
        53.5,
        54.8,
        55.97142227,
        56.63693345,
        57.29471271,
        57.94502337,
        58.58811413,
        59.22422017,
        59.8535642,
        60.47635732,
        61.09279988,
        61.7030822,
        62.30738531,
        62.90588152,
        63.49873503,
        64.08610243,
        64.66813322,
        65.24497022,
        65.81674998,
        66.38360318,
        66.94565497,
        67.50302528,
        71.5,
        75.8,
        80.0,
        84.0,
        87.6
    ]

    n_repeats = 4
    currents = sorted(sorted(set(currents)) * n_repeats)

    energy1 = 694
    energy2 = 400

#    und1.set(energy1).wait()
#    und2.set(energy2).wait()

    for i in range(100):

        for I in currents:
            chic_delay.set(I).wait()

            printable_I = str(round(I, 1)).replace(".", "_")

            fname = f"overnight04_{i:02}_{energy1}eV_{energy2}eV_{printable_I}A"
            print(fname)

            while check_intensity.wants_repeat():
                daq.acquire(fname, n_pulses=5000).wait()

        cycle_magnet().wait()















def overnight():
    currents = [
        5.0,
        11.9335371,
        16.8765438,
        20.6694283,
        23.8669628,
        26.6840341,
        29.2308494,
        31.5728768,
        33.7527725,
        35.8001658,
        37.7366295,
        39.5784494,
        41.3382779,
        43.0261768,
        44.6503044,
        46.217384,
        47.7330347,
        49.2020095,
        50.6283713,
        52.015626,
        53.5,
        54.8,
        55.97142227,
        56.63693345,
        57.29471271,
        57.94502337,
        58.58811413,
        59.22422017,
        59.8535642,
        60.47635732,
        61.09279988,
        61.7030822,
        62.30738531,
        62.90588152,
        63.49873503,
        64.08610243,
        64.66813322,
        65.24497022,
        65.81674998,
        66.38360318,
        66.94565497,
        67.50302528,
        71.5,
        75.8,
        80.0,
        84.0,
        87.6
    ]

    n_repeats = 4
    currents = sorted(sorted(set(currents)) * n_repeats)

    energy1 = 694
    energy2 = 802

#    und1.set(energy1).wait()
#    und2.set(energy2).wait()

    for i in range(100):

        for I in currents:
            chic_delay.set(I).wait()

            printable_I = str(round(I, 1)).replace(".", "_")

            fname = f"overnight05_{i:02}_{energy1}eV_{energy2}eV_{printable_I}A"
            print(fname)

            while check_intensity.wants_repeat():
                daq.acquire(fname, n_pulses=5000).wait()

        cycle_magnet().wait()












def overnight():
    currents = [
        12.215845980560607,
        15.116978121263239,
        17.54476888886744,
        19.67522697511649,
        21.5965263643961,
        23.360331736727083,
        25.000000000559517,
        26.53855034731866,
        27.99266086653778,
        29.374874017571226,
        30.694904105845765,
        31.960456910631603,
        33.1777670602658,
        34.351963577900015,
        35.487326322776916,
        36.58747068012243,
        37.65548360246898,
        38.69402580811058,
        39.705409889744644,
        40.69166092840929,
        41.65456418047311,
        42.59570305039474,
        43.51648966798437,
        43.51648966798437,
        45.30194303090119,
        47.01963659081147,
        48.67674415296662,
        50.27925646763386,
        51.83223759394742,
        53.340013971321795,
        54.80631661030178,
        56.23438986738845,
        57.627075933030426,
        58.98688134605165,
        60.316029997839784,
        61.61650584043036,
        62.89008763501911,
        64.13837748961751,
        65.36282448583248,
        66.56474439250431,
        67.74533622946402
    ]

    n_repeats = 4
    currents = sorted(sorted(set(currents)) * n_repeats)

    energy1 = 693
    energy2 = 770

#    und1.set(energy1).wait()
#    und2.set(energy2).wait()

    for i in range(100):

        for I in currents:
            chic_delay.set(I).wait()

            printable_I = str(round(I, 1)).replace(".", "_")

            fname = f"overnight07_{i:02}_{energy1}eV_{energy2}eV_{printable_I}A"
            print(fname)

            while check_intensity.wants_repeat():
                daq.acquire(fname, n_pulses=5000).wait()

        cycle_magnet().wait()








def overnight():
    currents = [
        12.215845980560607,
        15.116978121263239,
        17.54476888886744,
        19.67522697511649,
        21.5965263643961,
        23.360331736727083,
        25.000000000559517,
        26.53855034731866,
        27.99266086653778,
        29.374874017571226,
        30.694904105845765,
        31.960456910631603,
        33.1777670602658,
        34.351963577900015,
        35.487326322776916,
        36.58747068012243,
        37.65548360246898,
        38.69402580811058,
        39.705409889744644,
        40.69166092840929,
        41.65456418047311,
        42.59570305039474,
        43.51648966798437,
        43.51648966798437,
        45.30194303090119,
        47.01963659081147,
        48.67674415296662,
        50.27925646763386,
        51.83223759394742,
        53.340013971321795,
        54.80631661030178,
        56.23438986738845,
        57.627075933030426,
        58.98688134605165,
        60.316029997839784,
        61.61650584043036,
        62.89008763501911,
        64.13837748961751,
        65.36282448583248,
        66.56474439250431,
        67.74533622946402
    ]

    n_repeats = 4
    currents = sorted(sorted(set(currents)) * n_repeats)

    energy1 = 693
    energy2 = 800

#    und1.set(energy1).wait()
#    und2.set(energy2).wait()

    for i in range(4):

        for I in currents:
            chic_delay.set(I).wait()

            printable_I = str(round(I, 1)).replace(".", "_")

            fname = f"overnight08_{i:02}_{energy1}eV_{energy2}eV_{printable_I}A"
            print(fname)

            while check_intensity.wants_repeat():
                daq.acquire(fname, n_pulses=5000).wait()

        cycle_magnet().wait()


#def holo_scan():

#    energies = [
#        528.5,
#        529.2,
#        529.7,
#        530.2,
#        531.0,
#     ]

#    n_repeats = 100
#    energies = sorted(sorted(set(energies)) * n_repeats)

#    for E in energies:
#        
#        mono.set(E).wait()
#        
#        printable_E = str(round(E, 1)).replace(".", "_")

#        fname = f"holo_scan_{printable_E}eV"
#        print(fname)

#        while check_intensity.wants_repeat():
#            daq.acquire(fname, n_pulses=100).wait()

def holo_scan():

    energies = [
        528.5,
        529.2,
        529.7,
        530.2,
        531.0,
     ]

    n_repeats = 1
    reps = 100
   
    energies = sorted(sorted(set(energies)) * n_repeats)

    for E in energies:

        mono.set(E).wait()

        for i in range(reps):
            
            printable_E = str(round(E, 1)).replace(".", "_")

            fname = f"holo_scan_{i:03}_{printable_E}eV"
            print(fname)

            while check_intensity.wants_repeat():
                daq.acquire(fname, n_pulses=100, is_scan_step=(i != 0)).wait()


###

def holo_scan():

    energies = [
        528.5,
        529.2,
        529.7,
        530.2,
        531.0,
     ]

    n_repeats = 1
    reps = 5
   
    energies = sorted(sorted(set(energies)) * n_repeats)

    for i in range(reps):
        
        for E in energies:

            mono.set(E).wait()

            
            printable_E = str(round(E, 1)).replace(".", "_")

            fname = f"holo_scan_{i:03}_{printable_E}eV"
            print(fname)

            while check_intensity.wants_repeat():
                daq.acquire(fname, n_pulses=100).wait()






