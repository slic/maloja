#!/usr/bin/env python

import base64
import numpy as np
import h5py
from datetime import datetime, timedelta
from cam_server_client import PipelineClient


start = datetime.now() - timedelta(days=10)
cam = "SATES21-CAMS-PATT1"


pc = PipelineClient("http://sf-daqsync-01:8889")

bkg_names = pc.get_backgrounds(cam)
bkg_names.sort()

#fmt = "20221209_220537_978799"
fmt = "%Y%m%d_%H%M%S_%f"

N = len(cam) + 1

bkgs = {}
for bn in bkg_names:
    ts = bn[N:]
    dt = datetime.strptime(ts, fmt)
    if dt >= start:
        res = pc.get_background_image_bytes(bn)
        bytes = res["bytes"]
        dtype = res["dtype"]
        shape = res["shape"]
        bytes = base64.b64decode(bytes.encode())
        array = np.frombuffer(bytes, dtype=dtype)
        array.shape = shape
        bkgs[ts] = array
        print(array.shape, array[0, :3])
        break

with h5py.File("bkgs.h5", "w") as f:
    for ts, dat in bkgs.items():
        f[f"{cam}/{ts}"] = dat



